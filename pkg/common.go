package pkg

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/parnurzeal/gorequest"
)

const (
	MONTH_FORMAT     = "2006-01"
	DATE_FORMAT      = "2006-01-02"
	DATE_TIME_FORMAT = "2006-01-02 15:04:05"
	JST              = "Asia/Tokyo"
)

var dayOfWeekOfJapanese = []string{"日", "月", "火", "水", "木", "金", "土"}

func ApiResponseWithoutCred(origin string, status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	origins := map[string]bool{"https://dev.d7r69723er9j4.amplifyapp.com": true,
		"https://master.d7r69723er9j4.amplifyapp.com": true,
		"https://localhost:3000":                      true,
		"http://localhost:3000":                       true,
		"https://rc-booking.com":                      true,
		"https://yoyaku.rc-booking.com":               true,
		"https://kanri.rc-booking.com":                true,
		"https://test-yoyaku.rc-booking.com":          true,
		"https://dev-yoyaku.rc-booking.com":           true,
		"https://test-kanri.rc-booking.com":           true,
		"https://preview-test-kanri.rc-booking.com":   true,
		"https://preview-test-yoyaku.rc-booking.com":  true,
		"https://dev-kanri.rc-booking.com":            true,
	}

	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	fmt.Println("origin :", origin)
	if origins[origin] == true {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":  origin,
			"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                 "application/json",
		}
	} else {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                 "application/json",
		}
	}

	fmt.Println("resp.Headers :", resp.Headers)

	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return resp, nil
}

func ApiResponse(origin string, status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	origins := map[string]bool{"https://dev.d7r69723er9j4.amplifyapp.com": true,
		"https://master.d7r69723er9j4.amplifyapp.com": true,
		"https://localhost:3000":                      true,
		"http://localhost:3000":                       true,
		"https://rc-booking.com":                      true,
		"https://yoyaku.rc-booking.com":               true,
		"https://kanri.rc-booking.com":                true,
		"https://test-yoyaku.rc-booking.com":          true,
		"https://dev-yoyaku.rc-booking.com":           true,
		"https://test-kanri.rc-booking.com":           true,
		"https://dev-kanri.rc-booking.com":            true,
		"https://preview-test-kanri.rc-booking.com":   true,
		"https://preview-test-yoyaku.rc-booking.com":  true,
	}

	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	fmt.Println("origin :", origin)
	if origins[origin] == true {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":      origin,
			"Access-Control-Allow-Headers":     "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods":     "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                     "application/json",
			"Access-Control-Allow-Credentials": "true",
		}
	} else {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                 "application/json",
		}
	}

	fmt.Println("resp.Headers :", resp.Headers)

	if status >= 400 {
		fmt.Println("error content :", body)
	}

	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return resp, nil
}

func ApiResponseGuest(status int, body interface{}) (events.APIGatewayProxyResponse, error) {

	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
		"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
		"Content-Type":                 "application/json",
	}

	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return resp, nil
}

func Auth(structData interface{}, URL string) (*http.Response, error) {
	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(structData)
	req, _ := http.NewRequest("POST", URL, payloadBuf)

	client := &http.Client{}
	return client.Do(req)
}

func ApiResponseCSV(status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type",
		"Content-Type":                 "text/plain",
	}

	// fetch the elements form interface
	s := reflect.ValueOf(body).Elem()

	// find the type of element
	typeOfIface := s.Kind()
	// struct empty
	// slice 1 element
	// slice 3 element

	var buff1 string
	var buff2 string

	// if it is an slice of struct
	if typeOfIface == reflect.Slice {
		// iterate the slice of structs
		for i := 0; i < s.Len(); i++ {
			var buff1 string
			p := s.Index(i).Elem()
			// iterate struct element
			for j := 0; j < p.NumField(); j++ {
				// logic to change the struct values to csv

				// if it is a multilang struct we need only ja for temairazu
				tmp := p.Field(j).Interface()
				fmt.Println("p.Field(j).Type()) : ", p.Field(j).Type())
				if strings.Contains(fmt.Sprintf("%s", p.Field(j).Type()), "MultiLang") {
					// we assume only ja field is comming and index is zero
					tmp = p.Field(j).Field(0)
				}
				if j < p.NumField()-1 {
					buff1 = fmt.Sprintf("%s%v,", buff1, tmp)
				} else {
					buff1 = fmt.Sprintf("%s%v", buff1, tmp)
				}
			}
			// append all
			buff2 = fmt.Sprintf("%s%v\n", buff2, buff1)
		}
		resp.Body = buff2
		return resp, nil
	}

	if typeOfIface == reflect.Struct {
		// iterate struct element
		for j := 0; j < s.NumField(); j++ {
			// logic to change the struct values to csv
			// if it is a multilang struct we need only ja for temairazu
			tmp := s.Field(j).Interface()
			fmt.Println("p.Field(j).Type()) : ", s.Field(j).Type())
			if strings.Contains(fmt.Sprintf("%s", s.Field(j).Type()), "MultiLang") {
				// we assume only ja field is comming and index is zero
				tmp = s.Field(j).Field(0)
			}
			if j < s.NumField()-1 {
				buff1 = fmt.Sprintf("%s%v,", buff1, tmp)
			} else {
				buff1 = fmt.Sprintf("%s%v", buff1, tmp)
			}
		}
		resp.Body = buff1
		return resp, nil
	}

	resp.Body = string("Something went wrong in API response")
	return resp, nil
}

// this is working onl version, now new nersion is getting tested
func ApiResponseCSV1(status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type",
		"Content-Type":                 "text/plain",
	}

	// fetch the elements form interface
	s := reflect.ValueOf(body).Elem()

	// find the type of element
	typeOfIface := s.Kind()
	// struct empty
	// slice 1 element
	// slice 3 element

	var buff1 string
	var buff2 string

	// if it is an slice of struct
	if typeOfIface == reflect.Slice {
		// iterate the slice of structs
		for i := 0; i < s.Len(); i++ {
			var buff1 string
			p := s.Index(i).Elem()
			// iterate struct element
			for j := 0; j < p.NumField(); j++ {
				// logic to change the struct values to csv
				if j < p.NumField()-1 {
					buff1 = fmt.Sprintf("%s%v,", buff1, p.Field(j).Interface())
				} else {
					buff1 = fmt.Sprintf("%s%v", buff1, p.Field(j).Interface())
				}
			}
			// append all
			buff2 = fmt.Sprintf("%s%v\n", buff2, buff1)
		}
		resp.Body = buff2
		return resp, nil
	}

	if typeOfIface == reflect.Struct {

		// iterate struct element
		for j := 0; j < s.NumField(); j++ {
			// logic to change the struct values to csv
			if j < s.NumField()-1 {
				buff1 = fmt.Sprintf("%s%v,", buff1, s.Field(j).Interface())
			} else {
				buff1 = fmt.Sprintf("%s%v", buff1, s.Field(j).Interface())
			}
		}
		resp.Body = buff1
		return resp, nil
	}

	resp.Body = string("Something went wrong in API response")
	return resp, nil
}

func Dynamodb() *dynamodb.DynamoDB {
	// Environment variables
	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
	region := os.Getenv("AWS_REGION")

	// DynamoDB
	sess := session.Must(session.NewSession())
	config := aws.NewConfig().WithRegion(region)
	if len(endpoint) > 0 {
		config = config.WithEndpoint(endpoint)
	}

	return dynamodb.New(sess, config)
}

func DateString(t string) string {
	//set timezone,
	loc, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		return time.Now().Format("2006-01-02 15:04:05")
	}

	if t == "datetime" {
		return time.Now().In(loc).Format("2006-01-02 15:04:05")
	}

	if t == "date" {
		return time.Now().In(loc).Format("2006-01-02")
	}

	if t == "time" {
		return time.Now().In(loc).Format("15:04:05")
	}

	return time.Now().In(loc).Format("2006-01-02 15:04:05")
}

// func DateString() string {
// 	return time.Now().Format("2006-01-02 15:04:05")
// }

func DateTimeStamp() int64 {
	//set timezone,
	loc, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		return time.Now().Unix()
	}
	return time.Now().In(loc).Unix()
}

func S3() (*s3.S3, *session.Session) {
	region := os.Getenv("AWS_REGION")

	sess := session.Must(session.NewSession())
	config := aws.NewConfig().WithRegion(region)

	return s3.New(sess, config), sess
}

func CreateBucket(sss *s3.S3, bucketName string) (*s3.CreateBucketOutput, error) {
	resp, err := sss.CreateBucket(&s3.CreateBucketInput{
		Bucket: aws.String(bucketName),
	})
	return resp, err
}

func IsValidImageExt(category string) bool {
	switch category {
	case
		"jpg",
		"jpeg",
		"png":
		return true
	}
	return false
}

func GetStringInBetweenTwoString(str string, startS string, endS string) (result string, found bool) {
	s := strings.Index(str, startS)
	if s == -1 {
		return result, false
	}
	newS := str[s+len(startS):]
	e := strings.Index(newS, endS)
	if e == -1 {
		return result, false
	}
	result = newS[:e]
	return result, true
}

func UploadImageToS3(sess *session.Session, bucketName string, imageName string, imageDataBase64 string, path string) (*s3manager.UploadOutput, error) {
	// need to replace "data:.+;base64," from imageDataBase64 string
	m1 := regexp.MustCompile(`^data:.+;base64,`)
	imageData := m1.ReplaceAllString(imageDataBase64, "")

	// then decode it and convert to binary form
	data, _ := base64.StdEncoding.DecodeString(imageData)
	wb := new(bytes.Buffer)
	wb.Write(data)

	uploader := s3manager.NewUploader(sess)

	var empty *s3manager.UploadOutput

	ext, errs := GetStringInBetweenTwoString(imageDataBase64, "data:image/", ";base64")
	if errs == false {
		return empty, errors.New("Invalid image")
	}

	if IsValidImageExt(ext) == false {
		return empty, errors.New("Invalid image extension")
	}

	res, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:             aws.String(bucketName),
		Key:                aws.String(path + "." + ext),
		Body:               wb,
		ContentType:        aws.String("image/" + ext),
		ContentDisposition: aws.String("attachment"),
		ACL:                aws.String("public-read"),
		ContentEncoding:    aws.String("base64"),
	})

	if err != nil {
		return empty, err
	}

	return res, nil
}

func UploadFileToS3(sess *session.Session, bucketName string, fileName string, imageDataBase64 string, path string) (*s3manager.UploadOutput, error) {

	var empty *s3manager.UploadOutput
	validBase64Str := strings.Contains(imageDataBase64, ";base64,")
	if !validBase64Str {
		return empty, fmt.Errorf("invalid base64 string format")
	}

	// need to replace "data:.+;base64," from imageDataBase64 string
	m1 := regexp.MustCompile(`^data:.+;base64,`)
	fileData := m1.ReplaceAllString(imageDataBase64, "")

	// then decode it and convert to binary form
	data, _ := base64.StdEncoding.DecodeString(fileData)
	wb := new(bytes.Buffer)
	wb.Write(data)

	uploader := s3manager.NewUploader(sess)

	// nameSplit := strings.Split(fileName, ".")
	// ext := nameSplit[len(nameSplit) - 1]

	// ext, errs := GetStringInBetweenTwoString(imageDataBase64, "data:image/", ";base64")
	// if errs == false {
	// 	return empty, errors.New("Invalid image")
	// }

	// if IsValidImageExt(ext) == false {
	// 	return empty, errors.New("Invalid image extension")
	// }

	res, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(path + "/" + fileName),
		Body:   wb,
		// ContentType:        aws.String("image/" + ext),
		ContentDisposition: aws.String("attachment"),
		ACL:                aws.String("public-read"),
		ContentEncoding:    aws.String("base64"),
	})

	if err != nil {
		return empty, err
	}

	return res, nil
}

func GetTMError(m string) *NG {
	Ng := new(NG)
	Ng.Response = "NG"
	Ng.Error = m
	return Ng
}

// SendSlackNotification will post to an 'Incoming Webook' url setup in Slack Apps. It accepts
// some text and the slack channel is saved within Slack.

type Field struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

type Action struct {
	Type  string `json:"type"`
	Text  string `json:"text"`
	Url   string `json:"url"`
	Style string `json:"style"`
}

type Attachment struct {
	Fallback     *string   `json:"fallback"`
	Color        *string   `json:"color"`
	PreText      *string   `json:"pretext"`
	AuthorName   *string   `json:"author_name"`
	AuthorLink   *string   `json:"author_link"`
	AuthorIcon   *string   `json:"author_icon"`
	Title        *string   `json:"title"`
	TitleLink    *string   `json:"title_link"`
	Text         *string   `json:"text"`
	ImageUrl     *string   `json:"image_url"`
	Fields       []*Field  `json:"fields"`
	Footer       *string   `json:"footer"`
	FooterIcon   *string   `json:"footer_icon"`
	Timestamp    *int64    `json:"ts"`
	MarkdownIn   *[]string `json:"mrkdwn_in"`
	Actions      []*Action `json:"actions"`
	CallbackID   *string   `json:"callback_id"`
	ThumbnailUrl *string   `json:"thumb_url"`
}

type Payload struct {
	Parse       string       `json:"parse,omitempty"`
	Username    string       `json:"username,omitempty"`
	IconUrl     string       `json:"icon_url,omitempty"`
	IconEmoji   string       `json:"icon_emoji,omitempty"`
	Channel     string       `json:"channel,omitempty"`
	Text        string       `json:"text,omitempty"`
	LinkNames   string       `json:"link_names,omitempty"`
	Attachments []Attachment `json:"attachments,omitempty"`
	UnfurlLinks bool         `json:"unfurl_links,omitempty"`
	UnfurlMedia bool         `json:"unfurl_media,omitempty"`
	Markdown    bool         `json:"mrkdwn,omitempty"`
}

func (attachment *Attachment) AddField(field Field) *Attachment {
	attachment.Fields = append(attachment.Fields, &field)
	return attachment
}

func (attachment *Attachment) AddAction(action Action) *Attachment {
	attachment.Actions = append(attachment.Actions, &action)
	return attachment
}

func redirectPolicyFunc(req gorequest.Request, via []gorequest.Request) error {
	return fmt.Errorf("Incorrect token (redirection)")
}

func Send(webhookUrl string, proxy string, payload Payload) []error {
	fmt.Println("webhookUrl : ", webhookUrl)
	request := gorequest.New().Proxy(proxy)
	resp, _, err := request.
		Post(webhookUrl).
		RedirectPolicy(redirectPolicyFunc).
		Send(payload).
		End()

	if err != nil {
		return err
	}
	if resp.StatusCode >= 400 {
		return []error{fmt.Errorf("Error sending msg. Status: %v", resp.Status)}
	}

	return nil
}

func SendSlackNotification(fileName string, line int, functionName string, msg string) error {

	fmt.Println("err: " + msg)

	webhookUrl := "https://hooks.slack.com/services/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"
	channelName := "#rcbooking-notifications"

	payload := Payload{
		Text:      "File name : " + fileName + "\nLine Number : " + strconv.Itoa(line) + "\nFunction name : " + functionName + "\nMessage   : " + msg,
		Username:  "RCB",
		Channel:   channelName,
		IconEmoji: ":rcb:",
	}

	fmt.Printf("payload: %v\n", payload)

	err1 := Send(webhookUrl, "", payload)

	for _, e := range err1 {
		fmt.Printf("error: %v\n", e.Error())
	}

	return nil
}

func GetCode(n int, isAlpha bool) string {
	// generate random string
	var src = rand.NewSource(time.Now().UnixNano())

	var letterBytes = ""
	if isAlpha == true {
		letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	} else {
		letterBytes = "1234567890"
	}

	const (
		letterIdxBits = 6                    // 6 bits to represent a letter index
		letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
		letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
	)

	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func GetAdvanceCode(n int, isAlpha bool) string {
	// generate random string
	var src = rand.NewSource(time.Now().UnixNano())

	var letterBytes = ""
	if isAlpha == true {
		letterBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	} else {
		letterBytes = "1234567890"
	}

	const (
		letterIdxBits = 6                    // 6 bits to represent a letter index
		letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
		letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
	)

	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func GetFacilityCode() string {
	return (GetAdvanceCode(1, true) + GetAdvanceCode(7, false))
}

func SendMail(structData interface{}, URL string) (*http.Response, error) {
	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(structData)
	req, err := http.NewRequest("POST", URL, payloadBuf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	fmt.Println("payloadBuf :", payloadBuf)
	client := &http.Client{}
	return client.Do(req)
}

func GetCountryFromPhoneNumberCode(code string) string {
	countryCodes := map[string]string{
		"1403":  "CA",
		"1587":  "CA",
		"1780":  "CA",
		"1825":  "CA",
		"1236":  "CA",
		"1250":  "CA",
		"1604":  "CA",
		"1672":  "CA",
		"1778":  "CA",
		"1204":  "CA",
		"1431":  "CA",
		"1506":  "CA",
		"1709":  "CA",
		"1782":  "CA",
		"1902":  "CA",
		"1548":  "CA",
		"1249":  "CA",
		"1289":  "CA",
		"1343":  "CA",
		"1365":  "CA",
		"1387":  "CA",
		"1416":  "CA",
		"1437":  "CA",
		"1519":  "CA",
		"1613":  "CA",
		"1647":  "CA",
		"1705":  "CA",
		"1742":  "CA",
		"1807":  "CA",
		"1905":  "CA",
		"1418":  "CA",
		"1438":  "CA",
		"1450":  "CA",
		"1514":  "CA",
		"1579":  "CA",
		"1581":  "CA",
		"1819":  "CA",
		"1873":  "CA",
		"1306":  "CA",
		"1639":  "CA",
		"1867":  "CA",
		"1":     "US",
		"1242":  "BS",
		"1246":  "BB",
		"1264":  "AI",
		"1268":  "AG",
		"1284":  "VG",
		"1340":  "VI",
		"1345":  "KY",
		"1441":  "BM",
		"1473":  "GD",
		"1649":  "TC",
		"1664":  "MS",
		"1670":  "MP",
		"1671":  "GU",
		"1684":  "AS",
		"1721":  "SX",
		"1758":  "LC",
		"1767":  "DM",
		"1784":  "VC",
		"1787":  "PR",
		"1809":  "DO",
		"1829":  "DO",
		"1849":  "DO",
		"1868":  "TT",
		"1869":  "KN",
		"1876":  "JM",
		"1939":  "PR",
		"20":    "EG",
		"211":   "SS",
		"212":   "MA",
		"213":   "DZ",
		"216":   "TN",
		"218":   "LY",
		"220":   "GM",
		"221":   "SN",
		"222":   "MR",
		"223":   "ML",
		"224":   "GN",
		"225":   "CI",
		"226":   "BF",
		"227":   "NE",
		"228":   "TG",
		"229":   "BJ",
		"230":   "MU",
		"231":   "LR",
		"232":   "SL",
		"233":   "GH",
		"234":   "NG",
		"235":   "TD",
		"236":   "CF",
		"237":   "CM",
		"238":   "CV",
		"239":   "ST",
		"240":   "GQ",
		"241":   "GA",
		"242":   "CG",
		"243":   "CD",
		"244":   "AO",
		"245":   "GW",
		"246":   "IO",
		"247":   "SH",
		"248":   "SC",
		"249":   "SD",
		"250":   "RW",
		"251":   "ET",
		"252":   "SO",
		"253":   "DJ",
		"254":   "KE",
		"255":   "TZ",
		"256":   "UG",
		"257":   "BI",
		"258":   "MZ",
		"260":   "ZM",
		"261":   "MG",
		"262":   "RE",
		"263":   "ZW",
		"264":   "NA",
		"265":   "MW",
		"266":   "LS",
		"267":   "BW",
		"268":   "SZ",
		"269":   "KM",
		"27":    "ZA",
		"290":   "SH",
		"291":   "ER",
		"297":   "AW",
		"298":   "FO",
		"299":   "GL",
		"30":    "GR",
		"31":    "NL",
		"32":    "BE",
		"33":    "FR",
		"34":    "ES",
		"350":   "GI",
		"351":   "PT",
		"352":   "LU",
		"353":   "IE",
		"354":   "IS",
		"355":   "AL",
		"356":   "MT",
		"357":   "CY",
		"358":   "FI",
		"359":   "BG",
		"36":    "HU",
		"370":   "LT",
		"371":   "LV",
		"372":   "EE",
		"373":   "MD",
		"374":   "AM",
		"375":   "BY",
		"376":   "AD",
		"377":   "MC",
		"378":   "SM",
		"379":   "VA",
		"380":   "UA",
		"381":   "RS",
		"382":   "ME",
		"385":   "HR",
		"386":   "SI",
		"387":   "BA",
		"389":   "MK",
		"39":    "IT",
		"39066": "VA",
		"40":    "RO",
		"41":    "CH",
		"420":   "CZ",
		"421":   "SK",
		"423":   "LI",
		"43":    "AT",
		"44":    "GB",
		"45":    "DK",
		"46":    "SE",
		"47":    "NO",
		"4779":  "SJ",
		"48":    "PL",
		"49":    "DE",
		"500":   "FK",
		"501":   "BZ",
		"502":   "GT",
		"503":   "SV",
		"504":   "HN",
		"505":   "NI",
		"506":   "CR",
		"507":   "PA",
		"508":   "PM",
		"509":   "HT",
		"51":    "PE",
		"52":    "MX",
		"53":    "CU",
		"54":    "AR",
		"55":    "BR",
		"56":    "CL",
		"57":    "CO",
		"58":    "VE",
		"590":   "GP",
		"591":   "BO",
		"592":   "GY",
		"593":   "EC",
		"594":   "GF",
		"595":   "PY",
		"596":   "MQ",
		"597":   "SR",
		"598":   "UY",
		"5997":  "BQ",
		"5999":  "CW",
		"60":    "MY",
		"61":    "AU",
		"62":    "ID",
		"63":    "PH",
		"64":    "NZ",
		"65":    "SG",
		"66":    "TH",
		"670":   "TL",
		"672":   "NF",
		"673":   "BN",
		"674":   "NR",
		"675":   "PG",
		"676":   "TO",
		"677":   "SB",
		"678":   "VU",
		"679":   "FJ",
		"680":   "PW",
		"681":   "WF",
		"682":   "CK",
		"683":   "NU",
		"685":   "WS",
		"686":   "KI",
		"687":   "NC",
		"688":   "TV",
		"689":   "PF",
		"690":   "TK",
		"691":   "FM",
		"692":   "MH",
		"7":     "RU",
		"76":    "KZ",
		"77":    "KZ",
		"81":    "JP",
		"82":    "KR",
		"84":    "VN",
		"850":   "KP",
		"852":   "HK",
		"853":   "MO",
		"855":   "KH",
		"856":   "LA",
		"86":    "CN",
		"880":   "BD",
		"886":   "TW",
		"90":    "TR",
		"91":    "IN",
		"92":    "PK",
		"93":    "AF",
		"94":    "LK",
		"95":    "MM",
		"960":   "MV",
		"961":   "LB",
		"962":   "JO",
		"963":   "SY",
		"964":   "IQ",
		"965":   "KW",
		"966":   "SA",
		"967":   "YE",
		"968":   "OM",
		"970":   "PS",
		"971":   "AE",
		"972":   "IL",
		"973":   "BH",
		"974":   "QA",
		"975":   "BT",
		"976":   "MN",
		"977":   "NP",
		"98":    "IR",
		"992":   "TJ",
		"993":   "TM",
		"994":   "AZ",
		"995":   "GE",
		"996":   "KG",
		"998":   "UZ",
	}

	c := countryCodes[code]
	if len(c) == 0 {
		return ""
	} else {
		return c
	}
}

func Contain(list []string, str string) bool {
	for _, element := range list {
		if element == str {
			return true
		}
	}
	return false
}

func ContainForList(target, search []string) bool {
	for _, t := range target {
		if Contain(search, t) {
			return true
		}
	}
	return false
}

func SetLocalTime() {
	loc, err := time.LoadLocation(JST)
	if err != nil {
		loc = time.FixedZone(JST, 9*60*60)
	}
	time.Local = loc
}

func GetDayOfWeek(dateStr string) (string, error) {
	date, err := time.Parse(DATE_FORMAT, dateStr)
	if err != nil {
		return "", err
	}

	return dayOfWeekOfJapanese[date.Weekday()], nil
}

// 月初を取得
func GetBeginningOfMonth(date time.Time) time.Time {
	return date.AddDate(0, 0, -date.Day()+1)
}
