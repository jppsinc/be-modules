package pkg

import (
	"log"
	"reflect"

	"github.com/go-playground/locales/ja"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	ja_translations "github.com/go-playground/validator/v10/translations/ja"
)

const (
	ErrorInvalidRequestData           = "invalid request data"
	ErrorFailedToFetchRecord          = "failed to fetch record"
	ErrorCouldNotMarshalItem          = "could not marshal item"
	ErrorCouldNotUnMarshalItem        = "could not unmarshal item: data type mismatch"
	ErrorCouldNotDynamoPutItem        = "could not insert record in dynamoDB"
	ErrorCouldNotDeleteItem           = "could not delete record in dynamoDB"
	ErrorInvalidUrlHotelIdNotFound    = "Invalid Url HotelId Not Found"
	ErrorInvalidUrlIdNotFound         = "Invalid Url Id Not Found"
	ErrorQueryStringParameterNotFound = "Query string parameter Not Found"
	ErrorQueryStringParameterInvalid  = "Invalid Query string parameter"
	ErrorPathParameterInvalid         = "Invalid path parameter"
	ErrorCouldNotUpdateItem           = "could not update item"
	ErrorExpressionBuilder            = "Expression builder failed"
	ErrorTransactWriteItems           = "Transact Write Items failed"
	ErrorBatchGetItem                 = "Batch getItem failed"
	ErrorRecordNotFound               = "Record not found"
	ErrorAuthAttribute                = "LoginID or LoginPass is incorrect"
)

// GroupHotel.FacilityType
const (
	MINPAKU = "Minpaku"
	NORMAL  = "Normal"
)

// Invoice.Category
const (
	HOTEL_GHA               = "HotelGHAFee"
	PORTAL_SALES_COMMISSION = "PortalSalesCommissionFee"
	PSINC_COMMISSION        = "PsincGHACommissionFee"
	INITIAL_SETTING_FEE     = "InitialSettingFee"
	ACCOUNT_MAINTENANCE     = "AccountMaintenanceFee"
	MONTHLY_FEE             = "MonthlyFee"
	HOTEL_PAY               = "HotelPay"
)

// SiteController
const (
	TL = "TL-Lincoln"
)

type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty" dynamo:"error"`
}

type EmptyStruct struct{}

type EmptyStructCHECK struct{} // NOT USED

type MultiLang struct {
	Ja   string `json:"ja" dynamo:"ja"`
	En   string `json:"en" dynamo:"en"`
	Ko   string `json:"ko" dynamo:"ko"`
	ZhCN string `json:"zh_CN" dynamo:"zh_CN"` // simplified //https://stackoverflow.com/questions/4892372/language-codes-for-simplified-chinese-and-traditional-chinese
	ZhTW string `json:"zh_TW" dynamo:"zh_TW"` // traditional
}

type Inquery struct {
	ID                string       `json:"id" dynamo:"id"`
	HotelID           string       `json:"hotel_id" dynamo:"hotel_id"`
	InchargePerson    string       `json:"incharge_person" dynamo:"incharge_person"`
	InchargeContactNo string       `json:"incharge_contact_no" dynamo:"incharge_contact_no"`
	InchargeEmail     string       `json:"incharge_email" dynamo:"incharge_email"`
	Inquery           string       `json:"inquery" dynamo:"inquery"`
	UploadFile        []UploadFile `json:"upload_file" dynamo:"upload_file"`
	ReferenceID       string       `json:"reference_id" dynamo:"reference_id"`
	FacilityName      string       `json:"facility_name" dynamo:"facility_name"`
	CreatedAt         int64        `json:"created_at" dynamo:"created_at"`
	UpdatedAt         int64        `json:"updated_at" dynamo:"updated_at"`
}

type PasswordResetInquiry struct {
	ID                   string `json:"id" dynamo:"id"`
	FacilityName         string `json:"facility_name" dynamo:"facility_name"`
	InquirerName         string `json:"inquirer_name" dynamo:"inquirer_name"`
	EmailID              string `json:"email_id" dynamo:"email_id"`
	Body                 string `json:"body" dynamo:"body"`
	Phone                string `json:"phone" dynamo:"phone"`
	EmailSentToBESupport bool   `json:"email_sent_to_be_support" dynamo:"email_sent_to_be_support"`
	Resolved             bool   `json:"resolved" dynamo:"resolved"`
	ReferenceID          string `json:"reference_id" dynamo:"reference_id"`
	CreatedAt            int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt            int64  `json:"updated_at" dynamo:"updated_at"`
}

type UploadFile struct {
	FileName string `json:"file_name" dynamo:"file_name"`
	FileData string `json:"file_data" dynamo:"file_data"`
	FileURL  string `json:"file_url" dynamo:"file_url"`
}

type ChildSetting struct {
	HotelID   string      `json:"hotel_id" dynamo:"hotel_id"`
	ChildInfo []ChildInfo `json:"child_info" dynamo:"child_info"`
	CreatedAt int64       `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64       `json:"updated_at" dynamo:"updated_at"`
}

type ChildInfo struct {
	Rank                       string    `json:"rank" dynamo:"rank"`
	Name                       MultiLang `json:"name" dynamo:"name"`
	Desc                       MultiLang `json:"desc" dynamo:"desc"`
	StartAge                   int       `json:"start_age" dynamo:"start_age"`
	EndAge                     int       `json:"end_age" dynamo:"end_age"`
	InRevenue                  string    `json:"in_revenue" dynamo:"in_revenue"`
	InCapacity                 bool      `json:"in_capacity" dynamo:"in_capacity"`
	ZeroYenSell                bool      `json:"zero_yen_sell" dynamo:"zero_yen_sell"`
	Amount                     float64   `json:"amount" dynamo:"amount"`
	Unit                       string    `json:"unit" dynamo:"unit"`
	PortalSiteChildSettingType string    `json:"portal_site_child_setting_type" dynamo:"portal_site_child_setting_type"`
	IsActive                   bool      `json:"is_active" dynamo:"is_active"`
}

type Hotel struct {
	HotelID                string    `json:"hotel_id" dynamo:"hotel_id"`
	Title                  MultiLang `json:"title" dynamo:"title"`
	FacilityCode           string    `json:"facility_code" dynamo:"facility_code"`
	Zip                    string    `json:"zip" dynamo:"zip"`
	Prefecture             MultiLang `json:"prefecture" dynamo:"prefecture"`
	Address1               MultiLang `json:"address1" dynamo:"address1"`
	Address2               MultiLang `json:"address2" dynamo:"address2"`
	Phone                  string    `json:"phone" dynamo:"phone"`
	Fax                    string    `json:"fax" dynamo:"fax"`
	Website                string    `json:"website" dynamo:"website"`
	FAQURL                 string    `json:"faq_url" dynamo:"faq_url"`
	AvailablePaymentMethod MultiLang `json:"available_payment_method" dynamo:"available_payment_method"`
	Latitude               string    `json:"latitude" dynamo:"latitude"`
	Longitude              string    `json:"longitude" dynamo:"longitude"`
	Email                  string    `json:"email" dynamo:"email"`
	StaynaviID             string    `json:"staynavi_id" dynamo:"staynavi_id"`
	CheckIn                string    `json:"check-in" dynamo:"check-in"`
	CheckInEnd             string    `json:"check-in-end" dynamo:"check-in-end"`
	CheckOut               string    `json:"check-out" dynamo:"check-out"`
	SpecialInformation     string    `json:"special_information" dynamo:"special_information"`
	Images                 []Image   `json:"images" dynamo:"images"`
	HeaderImage            Image     `json:"header_image" dynamo:"header_image"`
	RoomCount              int64     `json:"room_count" dynamo:"room_count"`
	CreatedAt              int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt              int64     `json:"updated_at" dynamo:"updated_at"`
}

type Image struct {
	ID       string `json:"id" dynamo:"id"`
	URL      string `json:"url" dynamo:"url"`
	Alt      string `json:"alt" dynamo:"alt"`
	IsHide   bool   `json:"is_hide" dynamo:"is_hide"`
	IsFromTL bool   `json:"is_from_tl" dynamo:"is_from_tl"`
}

type HotelDefaultSettingsResponse struct {
	HotelID                           string                `json:"hotel_id" dynamo:"hotel_id"`
	FacilityCode                      string                `json:"facility_code" dynamo:"facility_code"`
	SiteController                    PC                    `json:"site_controller" dynamo:"site_controller"`
	PaymentCompany                    PC                    `json:"payment_company" dynamo:"payment_company"`
	JtbReservationCompletionEmailSend bool                  `json:"jtb_reservation_completion_email_send" dynamo:"jtb_reservation_completion_email_send"`
	IsFacilitySwitchable              bool                  `json:"is_facility_switchable" dynamo:"is_facility_switchable"`
	SwitchableHotelList               []SwitchableHotelList `json:"switchable_hotel_list" dynamo:"switchable_hotel_list"`
	GoogleHotelAdsCPA                 float64               `json:"google_hotel_ads_cpa" dynamo:"google_hotel_ads_cpa"`
	GoogleHotelAdsCPARequested        float64               `json:"google_hotel_ads_cpa_requested" dynamo:"google_hotel_ads_cpa_requested"`
	SearchPeriodDays                  int64                 `json:"search_period_days" dynamo:"search_period_days"`
	Pax                               int64                 `json:"pax" dynamo:"pax"`
	Rooms                             int64                 `json:"rooms" dynamo:"rooms"`
	Months                            int64                 `json:"months" dynamo:"months"`
	PlansPerPage                      int64                 `json:"plans_per_page" dynamo:"plans_per_page"`
	Nights                            int64                 `json:"nights" dynamo:"nights"`
	TermsOfService                    MultiLang             `json:"terms_of_service" dynamo:"terms_of_service"`
	MaxRoomsPerBooking                int64                 `json:"max_rooms_per_booking" dynamo:"max_rooms_per_booking"` // initial value 5
	MaxSearchableAdult                int64                 `json:"max_searchable_adult" dynamo:"max_searchable_adult"`
	MaxSearchableChildren             int64                 `json:"max_searchable_children" dynamo:"max_searchable_children"`
	IsDayUsePlanSearchable            bool                  `json:"is_day_use_plan_searchable" dynamo:"is_day_use_plan_searchable"`
	CreatedAt                         int64                 `json:"created_at" dynamo:"created_at"`
	UpdatedAt                         int64                 `json:"updated_at" dynamo:"updated_at"`
}

type HotelDefaultSettings struct {
	HotelID                           string                `json:"hotel_id" dynamo:"hotel_id"`
	FacilityCode                      string                `json:"facility_code" dynamo:"facility_code"`
	FacilityType                      string                `json:"facility_type" dynamo:"facility_type"`
	SiteController                    PC                    `json:"site_controller" dynamo:"site_controller"`
	PaymentCompany                    PC                    `json:"payment_company" dynamo:"payment_company"`
	JtbReservationCompletionEmailSend bool                  `json:"jtb_reservation_completion_email_send" dynamo:"jtb_reservation_completion_email_send"`
	IsFacilitySwitchable              bool                  `json:"is_facility_switchable" dynamo:"is_facility_switchable"`
	SwitchableHotelList               []SwitchableHotelList `json:"switchable_hotel_list" dynamo:"switchable_hotel_list"`
	GoogleHotelAdsCPA                 float64               `json:"google_hotel_ads_cpa" dynamo:"google_hotel_ads_cpa"`
	GoogleHotelAdsCPARequested        float64               `json:"google_hotel_ads_cpa_requested" dynamo:"google_hotel_ads_cpa_requested"`
	SearchPeriodDays                  int64                 `json:"search_period_days" dynamo:"search_period_days"`
	Pax                               int64                 `json:"pax" dynamo:"pax"`
	Rooms                             int64                 `json:"rooms" dynamo:"rooms"`
	Months                            int64                 `json:"months" dynamo:"months"`
	PlansPerPage                      int64                 `json:"plans_per_page" dynamo:"plans_per_page"`
	Nights                            int64                 `json:"nights" dynamo:"nights"`
	TermsOfService                    MultiLang             `json:"terms_of_service" dynamo:"terms_of_service"`
	MaxRoomsPerBooking                int64                 `json:"max_rooms_per_booking" dynamo:"max_rooms_per_booking"` // initial value 5
	MaxSearchableAdult                int64                 `json:"max_searchable_adult" dynamo:"max_searchable_adult"`
	MaxSearchableChildren             int64                 `json:"max_searchable_children" dynamo:"max_searchable_children"`
	IsDayUsePlanSearchable            bool                  `json:"is_day_use_plan_searchable" dynamo:"is_day_use_plan_searchable"`
	JoinPortalSiteInfoList            []JoinPortalSiteInfo  `json:"join_portal_site_info_list" dynamo:"join_portal_site_info_list"`
	CreatedAt                         int64                 `json:"created_at" dynamo:"created_at"`
	UpdatedAt                         int64                 `json:"updated_at" dynamo:"updated_at"`
}

type JoinPortalSiteInfo struct {
	PortalSiteID   string `json:"portal_site_id" dynamo:"portal_site_id"`
	PortalSiteName string `json:"portal_site_name" dynamo:"portal_site_name"`
}

type PC struct {
	ID   string    `json:"id" dynamo:"id"`
	Name MultiLang `json:"name" dynamo:"name"`
}

type SwitchableHotelList struct {
	ID           string    `json:"id" dynamo:"id"`
	FacilityName MultiLang `json:"facility_name" dynamo:"facility_name"`
}

type PaymentCompany struct {
	ID          string    `json:"id" dynamo:"id"`
	Name        MultiLang `json:"name" dynamo:"name"`
	Description string    `json:"description" dynamo:"description"`
	IsAvailable bool      `json:"is_available" dynamo:"is_available"`
	CreatedAt   int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64     `json:"updated_at" dynamo:"updated_at"`
}

type SiteController struct {
	ID          string    `json:"id" dynamo:"id"`
	Name        MultiLang `json:"name" dynamo:"name"`
	Description string    `json:"description" dynamo:"description"`
	IsAvailable bool      `json:"is_available" dynamo:"is_available"`
	CreatedAt   int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64     `json:"updated_at" dynamo:"updated_at"`
}

type SalesDefaultSetting struct {
	HotelID                                           string   `json:"hotel_id" dynamo:"hotel_id"`
	CutOffDays                                        int      `json:"cut_off_days" dynamo:"cut_off_days"`
	CutOffTime                                        string   `json:"cut_off_time" dynamo:"cut_off_time"`
	ReservableTimeForTheDay                           int      `json:"reservable_time_for_the_day" dynamo:"reservable_time_for_the_day"`
	ReservablePeriod                                  int      `json:"reservable_period" dynamo:"reservable_period"`
	MinimumReservationCharge                          int      `json:"minimum_reservation_charges" dynamo:"minimum_reservation_charges"`
	CancellableBeforeDays                             int      `json:"cancellable_before_days" dynamo:"cancellable_before_days"`
	ContinuousNightUpsell                             bool     `json:"continuous_night_upsell" dynamo:"continuous_night_upsell"`
	MidnightFlag                                      bool     `json:"mid_night_flag" dynamo:"mid_night_flag"`
	MidnightReservationTime                           string   `json:"mid_night_reservation_time" dynamo:"mid_night_reservation_time"`
	CheckoutTimeDisplayInReservationNotificationEmail bool     `json:"checkout_time_display_in_reservation_notification_email" dynamo:"checkout_time_display_in_reservation_notification_email"`
	PageLanguages                                     []string `json:"page_languages" dynamo:"page_languages"`
	CreatedAt                                         int64    `json:"created_at" dynamo:"created_at"`
	UpdatedAt                                         int64    `json:"updated_at" dynamo:"updated_at"`
}

type Language struct {
	Japanese           bool `json:"japanese" dynamo:"japanese"`
	English            bool `json:"english" dynamo:"english"`
	Korean             bool `json:"korean" dynamo:"korean"`
	ChineseSimplified  bool `json:"chinese_simplified" dynamo:"chinese_simplified"`
	ChineseTraditional bool `json:"chinese_traditional" dynamo:"chinese_traditional"`
}

type Room struct {
	ID                string                 `json:"id" dynamo:"id"`
	HotelID           string                 `json:"hotel_id" dynamo:"hotel_id"`
	Code              string                 `json:"code" dynamo:"code"`
	Status            string                 `json:"status" dynamo:"status"`
	RoomType          MultiLang              `json:"room_type" dynamo:"room_type"`
	Description       MultiLang              `json:"description" dynamo:"description"`
	Category          MultiLang              `json:"category" dynamo:"category"`
	Size              float64                `json:"size" dynamo:"size"`
	IsSmoking         bool                   `json:"is_smoking" dynamo:"is_smoking"`
	SmokeComment      MultiLang              `json:"smoke_comment" dynamo:"smoke_comment"`
	MaxPax            int64                  `json:"max_pax" dynamo:"max_pax"`
	MaxAdult          int64                  `json:"max_adult" dynamo:"max_adult"`
	MaxChildren       int64                  `json:"max_children" dynamo:"max_children"`
	MinOccupancy      int64                  `json:"min_occupancy" dynamo:"min_occupancy"`
	BedCount          int64                  `json:"bed_count" dynamo:"bed_count"`
	Facilities        []MultiLang            `json:"facilities" dynamo:"facilities"`
	BathroomAminities []MultiLang            `json:"bathroom_aminities" dynamo:"bathroom_aminities"`
	PlanList          []string               `json:"plan_list" dynamo:"plan_list"`
	Beddings          []Bedding              `json:"beddings" dynamo:"beddings"`
	Image             []Image                `json:"image" dynamo:"image"`
	Order             int64                  `json:"order" dynamo:"order"`
	RoomCategoryList  []RoomCategoryList     `json:"room_category_list" dynamo:"room_category_list"`
	PortalSiteTagList []PortalSiteTagForRoom `json:"portal_site_tag_list" dynamo:"portal_site_tag_list"`
	CreatedAt         int64                  `json:"created_at" dynamo:"created_at"`
	UpdatedAt         int64                  `json:"updated_at" dynamo:"updated_at"`
}

type RoomCategoryList struct {
	ID   string    `json:"id" dynamo:"id"`
	Name MultiLang `json:"name" dynamo:"name"`
}

type Bedding struct {
	Type  MultiLang `json:"type" dynamo:"type"`
	Size  string    `json:"size" dynamo:"size"`
	Count int64     `json:"count" dynamo:"count"`
	Brand MultiLang `json:"brand" dynamo:"brand"`
}

// type HotelGroup struct {
// 	GroupID                   string    `json:"group_id" dynamo:"group_id"`
// 	GroupName                 string    `json:"group_name" dynamo:"group_name"`
// 	GroupDesc                 string    `json:"group_desc" dynamo:"group_desc"`
// 	SubGroupName              string    `json:"sub_group_name" dynamo:"sub_group_name"`
// 	SubGroupDesc              string    `json:"sub_group_desc" dynamo:"sub_group_desc"`
// 	Cid                       string    `json:"cid" dynamo:"cid"`
// 	PaymentCompanyName        MultiLang `json:"payment_company_name" dynamo:"payment_company_name"`
// 	BillingContactPersonName  MultiLang `json:"billing_contact_person_name" dynamo:"billing_contact_person_name"`
// 	ContactPersonDivisionName string    `json:"contact_person_division_name" dynamo:"contact_person_division_name"`
// 	ContactPersonTitle        string    `json:"contact_person_title" dynamo:"contact_person_title"`
// 	Email                     string    `json:"email" dynamo:"email"`
// 	Tel                       string    `json:"tel" dynamo:"tel"`
// 	Zip                       string    `json:"zip" dynamo:"zip"`
// 	Prefecture                MultiLang `json:"prefecture" dynamo:"prefecture"`
// 	Address1                  MultiLang `json:"address1" dynamo:"address1"`
// 	Address2                  MultiLang `json:"address2" dynamo:"address2"`
// 	DeletedBy                 string    `json:"deleted_by" dynamo:"deleted_by"`
// 	DeletedAt                 string    `json:"deleted_at" dynamo:"deleted_at"`
// 	CreatedBy                 string    `json:"created_by" dynamo:"created_by"`
// 	UpdatedBy                 string    `json:"updated_by" dynamo:"updated_by"`
// 	CreatedAt                 int64     `json:"created_at" dynamo:"created_at"`
// 	UpdatedAt                 int64     `json:"updated_at" dynamo:"updated_at"`
// }

type MultiLangName struct {
	Normal string `json:"normal" dynamo:"normal"`
	Kana   string `json:"kana" dynamo:"kana"`
}

type ConsumptionTax struct {
	ID           string     `json:"id" dynamo:"id"`
	TaxType      string     `json:"tax_type" dynamo:"tax_type"`
	Message      *MultiLang `json:"message,omitempty" dynamo:"message"`
	TaxName      MultiLang  `json:"tax_name" dynamo:"tax_name"`
	IsActive     bool       `json:"is_active" dynamo:"is_active"`
	IsCalculated bool       `json:"is_calculated" dynamo:"is_calculated"`
	URL          *string    `json:"url,omitempty" dynamo:"url"`
	CreatedAt    int64      `json:"created_at" dynamo:"created_at"`
	UpdatedAt    int64      `json:"updated_at" dynamo:"updated_at"`
	Period1      *Period    `json:"period1,omitempty" dynamo:"period1"`
	Period2      *Period    `json:"period2,omitempty" dynamo:"period2"`
	AppliedOn    *string    `json:"applied_on,omitempty" dynamo:"applied_on"`
}

type Period struct {
	StartDate *string `json:"start_date" dynamo:"start_date"`
	EndDate   *string `json:"end_date" dynamo:"end_date"`
	Amount    float64 `json:"amount" dynamo:"amount"`
	Unit      string  `json:"unit" dynamo:"unit"`
}

type Questionnaire struct {
	ID             string    `json:"id" dynamo:"id"`
	HotelID        string    `json:"hotel_id" dynamo:"hotel_id"`
	Type           string    `json:"type" dynamo:"type"`
	QuestionName   string    `json:"question_name" dynamo:"question_name"`
	Memo           string    `json:"memo" dynamo:"memo"`
	InputType      string    `json:"input_type" dynamo:"input_type"`
	Title          MultiLang `json:"title" dynamo:"title"`
	DisplayComment MultiLang `json:"display_comment" dynamo:"display_comment"`
	Required       bool      `json:"required" dynamo:"required"`
	Default        bool      `json:"default" dynamo:"default"`
	Attribute      Attribute `json:"attribute" dynamo:"attribute"`
	Order          int64     `json:"order" dynamo:"order"`
	CreatedAt      int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt      int64     `json:"updated_at" dynamo:"updated_at"`
}

type PostCancellationSetting struct {
	HotelID                                   string    `json:"hotel_id" dynamo:"hotel_id"`
	BankName                                  MultiLang `json:"bank_name" dynamo:"bank_name"`
	BankAddress                               MultiLang `json:"bank_address" dynamo:"bank_address"`
	SwiftCode                                 string    `json:"swift_code" dynamo:"swift_code"`
	BranchName                                MultiLang `json:"branch_name" dynamo:"branch_name"`
	BranchAddress                             string    `json:"branch_address" dynamo:"branch_address"`
	BranchCode                                string    `json:"branch_code" dynamo:"branch_code"`
	AccountType                               MultiLang `json:"account_type" dynamo:"account_type"`
	Account                                   string    `json:"account" dynamo:"account"`
	AccountName                               MultiLang `json:"account_name" dynamo:"account_name"`
	CancellationChargeAppliedOnServiceTax     bool      `json:"cancellation_charge_applied_on_service_tax" dynamo:"cancellation_charge_applied_on_service_tax"`
	CancellationChargeAppliedOnConsumptionTax bool      `json:"cancellation_charge_applied_on_consumption_tax" dynamo:"cancellation_charge_applied_on_consumption_tax"`
	DoNotDisplayCancellationBankAccount       bool      `json:"do_not_display_cancellation_bank_account" dynamo:"do_not_display_cancellation_bank_account"`
	CancellationFeeRoundingMethod             string    `json:"cancellation_fee_rounding_method" dynamo:"cancellation_fee_rounding_method"`
	CreatedAt                                 int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt                                 int64     `json:"updated_at" dynamo:"updated_at"`
}

type CancellationPolicy struct {
	ID                                        string    `json:"id" dynamo:"id"`
	HotelID                                   string    `json:"hotel_id" dynamo:"hotel_id"`
	Title                                     MultiLang `json:"title" dynamo:"title"`
	Description                               string    `json:"description" dynamo:"description"`
	Notes                                     MultiLang `json:"notes" dynamo:"notes"`
	Cancellable                               bool      `json:"cancellable" dynamo:"cancellable"`
	FeeCalculationMethod                      string    `json:"fee_calculation_method" dynamo:"fee_calculation_method"`
	CancellationCharge                        []CC      `json:"cancellation_charge" dynamo:"cancellation_charge"`
	CancellationChargeUnit                    string    `json:"cancellation_charge_unit" dynamo:"cancellation_charge_unit"`
	NoshowPanelty                             bool      `json:"noshow_penalty" dynamo:"noshow_penalty"`
	NoshowCharge                              float64   `json:"noshow_charge" dynamo:"noshow_charge"`
	NoshowChargeUnit                          string    `json:"noshow_charge_unit" dynamo:"noshow_charge_unit"`
	CancellationChargeAppliedOnServiceTax     bool      `json:"cancellation_charge_applied_on_service_tax" dynamo:"cancellation_charge_applied_on_service_tax"`
	CancellationChargeAppliedOnConsumptionTax bool      `json:"cancellation_charge_applied_on_consumption_tax" dynamo:"cancellation_charge_applied_on_consumption_tax"`
	CreatedAt                                 int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt                                 int64     `json:"updated_at" dynamo:"updated_at"`
}

type CC struct {
	DaysBeforeArrival      int64   `json:"days_before_arrival" dynamo:"days_before_arrival"`
	Amount                 float64 `json:"amount" dynamo:"amount"`
	CancellationChargeUnit string  `json:"cancellation_charge_unit" dynamo:"cancellation_charge_unit"` // used in TL only
}

// type Tax struct {
// 	ID           string  `json:"id" dynamo:"id"`
// 	HotelID      string  `json:"hotel_id" dynamo:"hotel_id"`
// 	TaxType      string  `json:"tax_type" dynamo:"tax_type"`
// 	Period1      Period  `json:"period1" dynamo:"period1"`
// 	Period2      Period  `json:"period2" dynamo:"period2"`
// 	TaxName      MultiLang `json:"tax_name" dynamo:"tax_name"`
// 	IsActive     int64   `json:"is_active" dynamo:"is_active"`
// 	IsCalculated int64   `json:"is_calculated" dynamo:"is_calculated"`
// 	CreatedAt    string  `json:"created_at" dynamo:"created_at"`
// 	UpdatedAt    string  `json:"updated_at" dynamo:"updated_at"`
// }
//
type PlanGroupDiscount struct {
	ID                           string                       `json:"id" dynamo:"id"`
	Name                         MultiLang                    `json:"name" dynamo:"name"`
	WithSpecialDiscount          bool                         `json:"with_special_discount" dynamo:"with_special_discount"`
	Unit                         string                       `json:"unit" dynamo:"unit"`
	Discount                     float64                      `json:"discount" dynamo:"discount"`
	DiscountAmount               float64                      `json:"discount_amount" dynamo:"discount_amount"`
	DiscountName                 MultiLang                    `json:"discount_name" dynamo:"discount_name"`
	DiscountPerRoomPerNight      [][]float64                  `json:"discount_per_room_per_night" dynamo:"discount_per_room_per_night"`
	DiscountPerRoomPerNightChild DiscountPerRoomPerNightChild `json:"discount_per_room_per_night_child" dynamo:"discount_per_room_per_night_child"`
}

type DiscountPerRoomPerNightChild struct {
	AdultTotalDiscount  [][]float64 `json:"adult_total_discount" dynamo:"adult_total_discount"`
	ChildATotalDiscount [][]float64 `json:"childA_total_discount" dynamo:"childA_total_discount"`
	ChildBTotalDiscount [][]float64 `json:"childB_total_discount" dynamo:"childB_total_discount"`
	ChildCTotalDiscount [][]float64 `json:"childC_total_discount" dynamo:"childC_total_discount"`
	ChildDTotalDiscount [][]float64 `json:"childD_total_discount" dynamo:"childD_total_discount"`
	ChildETotalDiscount [][]float64 `json:"childE_total_discount" dynamo:"childE_total_discount"`
	ChildFTotalDiscount [][]float64 `json:"childF_total_discount" dynamo:"childF_total_discount"`
}

type Booking struct {
	ID                           string                 `json:"id" dynamo:"id"`
	HotelID                      string                 `json:"hotel_id" dynamo:"hotel_id"`
	BookingReferenceID           string                 `json:"booking_reference_id" dynamo:"booking_reference_id"`
	PortalSiteID                 string                 `json:"portal_site_id" dynamo:"portal_site_id"`
	Status                       string                 `json:"status" dynamo:"status"`
	PlanID                       string                 `json:"plan_id" dynamo:"plan_id"`
	PlanName                     MultiLang              `json:"plan_name" dynamo:"plan_name"`
	PlanType                     string                 `json:"plan_type" dynamo:"plan_type"`
	Pricing                      string                 `json:"pricing" dynamo:"pricing"`
	PlanPriceID                  string                 `json:"plan_price_id" dynamo:"plan_price_id"`
	BookingDate                  string                 `json:"booking_date" dynamo:"booking_date"`
	BookingTime                  string                 `json:"booking_time" dynamo:"booking_time"`
	BookerName                   string                 `json:"booker_name" dynamo:"booker_name"`
	BookerNameKana               string                 `json:"booker_name_kana" dynamo:"booker_name_kana"`
	BookerCountry                string                 `json:"booker_country" dynamo:"booker_country"`
	BookerCountryCode            string                 `json:"booker_country_code" dynamo:"booker_country_code"`
	BookerContact                string                 `json:"booker_contact" dynamo:"booker_contact"`
	IsGuestBookerSame            bool                   `json:"is_guest_booker_same" dynamo:"is_guest_booker_same"`
	GuestName                    string                 `json:"guest_name" dynamo:"guest_name"`
	GuestNameKana                string                 `json:"guest_name_kana" dynamo:"guest_name_kana"`
	GuestCountry                 string                 `json:"guest_country" dynamo:"guest_country"`
	GuestCountryCode             string                 `json:"guest_country_code" dynamo:"guest_country_code"`
	GuestContact                 string                 `json:"guest_contact" dynamo:"guest_contact"`
	GuestEmail                   string                 `json:"guest_email" dynamo:"guest_email"`
	LastCheckInTime              string                 `json:"last_check_in_time" dynamo:"last_check_in_time"`
	CheckInDate                  string                 `json:"check_in_date" dynamo:"check_in_date"`
	CheckInTime                  string                 `json:"check_in_time" dynamo:"check_in_time"`
	CheckOutDate                 string                 `json:"check_out_date" dynamo:"check_out_date"`
	CheckOutTime                 string                 `json:"check_out_time" dynamo:"check_out_time"`
	Meal                         []Meal                 `json:"meal" dynamo:"meal"`
	GuestCardPassCode            string                 `json:"guest_card_pass_code" dynamo:"guest_card_pass_code"`
	IsGuestCardCreated           bool                   `json:"is_guest_card_created" dynamo:"is_guest_card_created"`
	RoomCount                    int64                  `json:"room_count" dynamo:"room_count"`
	Nights                       int64                  `json:"nights" dynamo:"nights"`
	AccomodationCharge           int64                  `json:"accomodation_charge" dynamo:"accomodation_charge"`
	ServiceCharge                int64                  `json:"service_charge" dynamo:"service_charge"`
	ConsumptionCharge            int64                  `json:"consumption_charge" dynamo:"consumption_charge"`
	TotalCharge                  int64                  `json:"total_charge" dynamo:"total_charge"`
	PriceAdult                   [][]int64              `json:"price_adult" dynamo:"price_adult"`
	PriceChildA                  [][]int64              `json:"price_childA" dynamo:"price_childA"`
	PriceChildB                  [][]int64              `json:"price_childB" dynamo:"price_childB"`
	PriceChildC                  [][]int64              `json:"price_childC" dynamo:"price_childC"`
	PriceChildD                  [][]int64              `json:"price_childD" dynamo:"price_childD"`
	PriceChildE                  [][]int64              `json:"price_childE" dynamo:"price_childE"`
	PriceChildF                  [][]int64              `json:"price_childF" dynamo:"price_childF"`
	PriceChildOthers             [][]int64              `json:"price_child_others" dynamo:"price_child_others"`
	BookerEmail                  string                 `json:"booker_email" dynamo:"booker_email"`
	GenderRestriction            string                 `json:"gender_restriction" dynamo:"gender_restriction"`
	RoomID                       string                 `json:"room_id" dynamo:"room_id"`
	RoomType                     MultiLang              `json:"room_type" dynamo:"room_type"`
	Adult                        []int64                `json:"adult" dynamo:"adult"`
	Child                        []int64                `json:"child" dynamo:"child"`
	ChildAge                     []int64                `json:"child_age" dynamo:"child_age"`
	ChildInfo                    []ChildInfo            `json:"child_info" dynamo:"child_info"`
	Cancellable                  bool                   `json:"cancellable" dynamo:"cancellable"`
	CancellationPolicy           BCP                    `json:"cancellation_policy" dynamo:"cancellation_policy"`
	QuestionAnswers              []QuestionAnswer       `json:"question_answers" dynamo:"question_answers"`
	PaymentMethod                string                 `json:"payment_method" dynamo:"payment_method"`
	CreditCardCompanyName        string                 `json:"credit_card_company_name" dynamo:"credit_card_company_name"`           // HotelPay/JTB
	CreditCardCompanyPlanName    string                 `json:"credit_card_company_plan_name" dynamo:"credit_card_company_plan_name"` // HotelPay/JTB
	GoogleClickId                string                 `json:"google_click_id" dynamo:"google_click_id"`
	DeviceUsed                   string                 `json:"device_used" dynamo:"device_used"`
	Source                       string                 `json:"source" dynamo:"source"`
	IsNoshow                     bool                   `json:"is_noshow" dynamo:"is_noshow"`
	CancellationCharge           float64                `json:"cancellation_charge" dynamo:"cancellation_charge"`
	CancellationChargeMethod     string                 `json:"cancellation_charge_method" dynamo:"cancellation_charge_method"`
	CancellationContactMethod    string                 `json:"cancellation_contact_method" dynamo:"cancellation_contact_method"`
	AvoidCancellationEmail       bool                   `json:"avoid_cancellation_email" dynamo:"avoid_cancellation_email"`
	RefundableAmount             float64                `json:"refundable_amount" dynamo:"refundable_amount"`
	PageLanguage                 string                 `json:"page_language" dynamo:"page_language"`
	BookingEmailSendTime         string                 `json:"booking_email_send_time" dynamo:"booking_email_send_time"`
	ConfirmationEmailSendHistory []string               `json:"confirmation_email_send_history" dynamo:"confirmation_email_send_history"`
	RemindEmailSendHistory       []string               `json:"remind_email_send_history" dynamo:"remind_email_send_history"`
	WelcomeEmailSendTime         string                 `json:"welcome_email_send_time" dynamo:"welcome_email_send_time"`
	ThankYouEmailSendTime        string                 `json:"thankyou_email_send_time" dynamo:"thankyou_email_send_time"`
	QuestionsForCustomer         []QuestionsForCustomer `json:"questions_for_customer" dynamo:"questions_for_customer"`
	ChildAServiceTaxList         [][]int64              `json:"child_A_service_tax_list" dynamo:"child_A_service_tax_list"`
	ChildAConsumptionTaxList     [][]int64              `json:"child_A_consumption_tax_list" dynamo:"child_A_consumption_tax_list"`
	ChildBServiceTaxList         [][]int64              `json:"child_B_service_tax_list" dynamo:"child_B_service_tax_list"`
	ChildBConsumptionTaxList     [][]int64              `json:"child_B_consumption_tax_list" dynamo:"child_B_consumption_tax_list"`
	ChildCServiceTaxList         [][]int64              `json:"child_C_service_tax_list" dynamo:"child_C_service_tax_list"`
	ChildCConsumptionTaxList     [][]int64              `json:"child_C_consumption_tax_list" dynamo:"child_C_consumption_tax_list"`
	ChildDServiceTaxList         [][]int64              `json:"child_D_service_tax_list" dynamo:"child_D_service_tax_list"`
	ChildDConsumptionTaxList     [][]int64              `json:"child_D_consumption_tax_list" dynamo:"child_D_consumption_tax_list"`
	ChildEServiceTaxList         [][]int64              `json:"child_E_service_tax_list" dynamo:"child_E_service_tax_list"`
	ChildEConsumptionTaxList     [][]int64              `json:"child_E_consumption_tax_list" dynamo:"child_E_consumption_tax_list"`
	ChildFServiceTaxList         [][]int64              `json:"child_F_service_tax_list" dynamo:"child_F_service_tax_list"`
	ChildFConsumptionTaxList     [][]int64              `json:"child_F_consumption_tax_list" dynamo:"child_F_consumption_tax_list"`
	ServiceTaxList               [][]int64              `json:"service_tax_list" dynamo:"service_tax_list"`
	ConsumptionTaxList           [][]int64              `json:"consumption_tax_list" dynamo:"consumption_tax_list"`
	PlanPriceIDList              [][]string             `json:"plan_price_id_list" dynamo:"plan_price_id_list"`
	OptionList                   []OptionList           `json:"option_list" dynamo:"option_list"`
	PlanGroupDiscountList        []PlanGroupDiscount    `json:"plan_group_discount_list" dynamo:"plan_group_discount_list"`
	CPARate                      float64                `json:"cpa_rate" dynamo:"cpa_rate"`
	PortalSiteCommissionRate     int                    `json:"portal_site_commission_rate" dynamo:"portal_site_commission_rate"`
	IsPaidAd                     bool                   `json:"is_paid_ad" dynamo:"is_paid_ad"` // if source is GoogleHotelAds ; IsPaidAd=1 means paid ad, IsPaidAd=0 means booking links
	SpecialCode                  string                 `json:"special_code" dynamo:"special_code"`
	CreatedAt                    int64                  `json:"created_at" dynamo:"created_at"`
	UpdatedAt                    int64                  `json:"updated_at" dynamo:"updated_at"`
	CancelledAt                  int64                  `json:"cancelled_at" dynamo:"cancelled_at"`
	CancelledBy                  string                 `json:"cancelled_by" dynamo:"cancelled_by"`
	UpdateCount                  int64                  `json:"update_count" dynamo:"update_count"`
	ConsecutiveNightsDiscount    [][]float64            `json:"consecutive_nights_discount" dynamo:"consecutive_nights_discount"` // cnd per room per night
	TransitionSource             string                 `json:"transition_source" dynamo:"transition_source"`                     // 本当に直接経由予約なのかを判断するため、遷移元のURLを保存
}

type BookingIndex struct {
	ID                           string                 `json:"id" dynamo:"id"`
	HotelID                      string                 `json:"hotel_id" dynamo:"hotel_id"`
	BookingReferenceID           string                 `json:"booking_reference_id" dynamo:"booking_reference_id"`
	PortalSiteID                 string                 `json:"portal_site_id" dynamo:"portal_site_id"`
	Status                       string                 `json:"status" dynamo:"status"`
	PlanID                       string                 `json:"plan_id" dynamo:"plan_id"`
	PlanName                     MultiLang              `json:"plan_name" dynamo:"plan_name"`
	PlanType                     string                 `json:"plan_type" dynamo:"plan_type"`
	Pricing                      string                 `json:"pricing" dynamo:"pricing"`
	PlanPriceID                  string                 `json:"plan_price_id" dynamo:"plan_price_id"`
	BookingDate                  string                 `json:"booking_date" dynamo:"booking_date"`
	BookingTime                  string                 `json:"booking_time" dynamo:"booking_time"`
	BookerName                   string                 `json:"booker_name" dynamo:"booker_name"`
	BookerNameKana               string                 `json:"booker_name_kana" dynamo:"booker_name_kana"`
	BookerCountry                string                 `json:"booker_country" dynamo:"booker_country"`
	BookerCountryCode            string                 `json:"booker_country_code" dynamo:"booker_country_code"`
	BookerContact                string                 `json:"booker_contact" dynamo:"booker_contact"`
	IsGuestBookerSame            bool                   `json:"is_guest_booker_same" dynamo:"is_guest_booker_same"`
	GuestName                    string                 `json:"guest_name" dynamo:"guest_name"`
	GuestNameKana                string                 `json:"guest_name_kana" dynamo:"guest_name_kana"`
	GuestCountry                 string                 `json:"guest_country" dynamo:"guest_country"`
	GuestCountryCode             string                 `json:"guest_country_code" dynamo:"guest_country_code"`
	GuestContact                 string                 `json:"guest_contact" dynamo:"guest_contact"`
	GuestEmail                   string                 `json:"guest_email" dynamo:"guest_email"`
	LastCheckInTime              string                 `json:"last_check_in_time" dynamo:"last_check_in_time"`
	CheckInDate                  string                 `json:"check_in_date" dynamo:"check_in_date"`
	CheckInTime                  string                 `json:"check_in_time" dynamo:"check_in_time"`
	CheckOutDate                 string                 `json:"check_out_date" dynamo:"check_out_date"`
	CheckOutTime                 string                 `json:"check_out_time" dynamo:"check_out_time"`
	Meal                         []Meal                 `json:"meal" dynamo:"meal"`
	GuestCardPassCode            string                 `json:"guest_card_pass_code" dynamo:"guest_card_pass_code"`
	IsGuestCardCreated           bool                   `json:"is_guest_card_created" dynamo:"is_guest_card_created"`
	RoomCount                    int64                  `json:"room_count" dynamo:"room_count"`
	Nights                       int64                  `json:"nights" dynamo:"nights"`
	AccomodationCharge           int64                  `json:"accomodation_charge" dynamo:"accomodation_charge"`
	ServiceCharge                int64                  `json:"service_charge" dynamo:"service_charge"`
	ConsumptionCharge            int64                  `json:"consumption_charge" dynamo:"consumption_charge"`
	TotalCharge                  int64                  `json:"total_charge" dynamo:"total_charge"`
	PriceAdult                   [][]int64              `json:"price_adult" dynamo:"price_adult"`
	PriceChildA                  [][]int64              `json:"price_childA" dynamo:"price_childA"`
	PriceChildB                  [][]int64              `json:"price_childB" dynamo:"price_childB"`
	PriceChildC                  [][]int64              `json:"price_childC" dynamo:"price_childC"`
	PriceChildD                  [][]int64              `json:"price_childD" dynamo:"price_childD"`
	PriceChildE                  [][]int64              `json:"price_childE" dynamo:"price_childE"`
	PriceChildF                  [][]int64              `json:"price_childF" dynamo:"price_childF"`
	PriceChildOthers             [][]int64              `json:"price_child_others" dynamo:"price_child_others"`
	BookerEmail                  string                 `json:"booker_email" dynamo:"booker_email"`
	GenderRestriction            string                 `json:"gender_restriction" dynamo:"gender_restriction"`
	RoomID                       string                 `json:"room_id" dynamo:"room_id"`
	RoomType                     MultiLang              `json:"room_type" dynamo:"room_type"`
	Adult                        []int64                `json:"adult" dynamo:"adult"`
	Child                        []int64                `json:"child" dynamo:"child"`
	ChildAge                     []int64                `json:"child_age" dynamo:"child_age"`
	ChildInfo                    []ChildInfo            `json:"child_info" dynamo:"child_info"`
	Cancellable                  bool                   `json:"cancellable" dynamo:"cancellable"`
	CancellationPolicy           BCP                    `json:"cancellation_policy" dynamo:"cancellation_policy"`
	QuestionAnswers              []QuestionAnswer       `json:"question_answers" dynamo:"question_answers"`
	PaymentMethod                string                 `json:"payment_method" dynamo:"payment_method"`
	CreditCardCompanyName        string                 `json:"credit_card_company_name" dynamo:"credit_card_company_name"`           // HotelPay/JTB
	CreditCardCompanyPlanName    string                 `json:"credit_card_company_plan_name" dynamo:"credit_card_company_plan_name"` // HotelPay/JTB
	GoogleClickId                string                 `json:"google_click_id" dynamo:"google_click_id"`
	DeviceUsed                   string                 `json:"device_used" dynamo:"device_used"`
	Source                       string                 `json:"source" dynamo:"source"`
	IsNoshow                     bool                   `json:"is_noshow" dynamo:"is_noshow"`
	CancellationCharge           float64                `json:"cancellation_charge" dynamo:"cancellation_charge"`
	CancellationChargeMethod     string                 `json:"cancellation_charge_method" dynamo:"cancellation_charge_method"`
	CancellationContactMethod    string                 `json:"cancellation_contact_method" dynamo:"cancellation_contact_method"`
	AvoidCancellationEmail       bool                   `json:"avoid_cancellation_email" dynamo:"avoid_cancellation_email"`
	RefundableAmount             float64                `json:"refundable_amount" dynamo:"refundable_amount"`
	PageLanguage                 string                 `json:"page_language" dynamo:"page_language"`
	BookingEmailSendTime         string                 `json:"booking_email_send_time" dynamo:"booking_email_send_time"`
	ConfirmationEmailSendHistory []string               `json:"confirmation_email_send_history" dynamo:"confirmation_email_send_history"`
	RemindEmailSendHistory       []string               `json:"remind_email_send_history" dynamo:"remind_email_send_history"`
	WelcomeEmailSendTime         string                 `json:"welcome_email_send_time" dynamo:"welcome_email_send_time"`
	ThankYouEmailSendTime        string                 `json:"thankyou_email_send_time" dynamo:"thankyou_email_send_time"`
	QuestionsForCustomer         []QuestionsForCustomer `json:"questions_for_customer" dynamo:"questions_for_customer"`
	ChildAServiceTaxList         [][]int64              `json:"child_A_service_tax_list" dynamo:"child_A_service_tax_list"`
	ChildAConsumptionTaxList     [][]int64              `json:"child_A_consumption_tax_list" dynamo:"child_A_consumption_tax_list"`
	ChildBServiceTaxList         [][]int64              `json:"child_B_service_tax_list" dynamo:"child_B_service_tax_list"`
	ChildBConsumptionTaxList     [][]int64              `json:"child_B_consumption_tax_list" dynamo:"child_B_consumption_tax_list"`
	ChildCServiceTaxList         [][]int64              `json:"child_C_service_tax_list" dynamo:"child_C_service_tax_list"`
	ChildCConsumptionTaxList     [][]int64              `json:"child_C_consumption_tax_list" dynamo:"child_C_consumption_tax_list"`
	ChildDServiceTaxList         [][]int64              `json:"child_D_service_tax_list" dynamo:"child_D_service_tax_list"`
	ChildDConsumptionTaxList     [][]int64              `json:"child_D_consumption_tax_list" dynamo:"child_D_consumption_tax_list"`
	ChildEServiceTaxList         [][]int64              `json:"child_E_service_tax_list" dynamo:"child_E_service_tax_list"`
	ChildEConsumptionTaxList     [][]int64              `json:"child_E_consumption_tax_list" dynamo:"child_E_consumption_tax_list"`
	ChildFServiceTaxList         [][]int64              `json:"child_F_service_tax_list" dynamo:"child_F_service_tax_list"`
	ChildFConsumptionTaxList     [][]int64              `json:"child_F_consumption_tax_list" dynamo:"child_F_consumption_tax_list"`
	ServiceTaxList               [][]int64              `json:"service_tax_list" dynamo:"service_tax_list"`
	ConsumptionTaxList           [][]int64              `json:"consumption_tax_list" dynamo:"consumption_tax_list"`
	PlanPriceIDList              [][]string             `json:"plan_price_id_list" dynamo:"plan_price_id_list"`
	OptionList                   []OptionList           `json:"option_list" dynamo:"option_list"`
	PlanGroupDiscountList        []PlanGroupDiscount    `json:"plan_group_discount_list" dynamo:"plan_group_discount_list"`
	CPARate                      float64                `json:"cpa_rate" dynamo:"cpa_rate"`
	PortalSiteCommissionRate     int                    `json:"portal_site_commission_rate" dynamo:"portal_site_commission_rate"`
	IsPaidAd                     bool                   `json:"is_paid_ad" dynamo:"is_paid_ad"` // if source is GoogleHotelAds ; IsPaidAd=1 means paid ad, IsPaidAd=0 means booking links
	CreatedAt                    int64                  `json:"created_at" dynamo:"created_at"`
	UpdatedAt                    int64                  `json:"updated_at" dynamo:"updated_at"`
	CancelledAt                  int64                  `json:"cancelled_at" dynamo:"cancelled_at"`
	CancelledBy                  string                 `json:"cancelled_by" dynamo:"cancelled_by"`
	TLBookingSyncStatus          string                 `json:"tl_booking_sync_status" dynamo:"tl_booking_sync_status"`               // Success, Error
	TLBookingSyncErrorMessage    string                 `json:"tl_booking_sync_error_message" dynamo:"tl_booking_sync_error_message"` // if there is any error
	UpdateCount                  int64                  `json:"update_count" dynamo:"update_count"`
	ConsecutiveNightsDiscount    [][]float64            `json:"consecutive_nights_discount" dynamo:"consecutive_nights_discount"` // cnd per room per night
}
type StaticInfo struct {
	Language   []string `json:"language" dynamo:"language"`
	Currencies []string `json:"currencies" dynamo:"currencies"`
}

type BCP struct {
	CancellationCharge                        []CC      `json:"cancellation_charge" dynamo:"cancellation_charge"`
	FeeCalculationMethod                      string    `json:"fee_calculation_method" dynamo:"fee_calculation_method"`
	CancellationChargeUnit                    string    `json:"cancellation_charge_unit" dynamo:"cancellation_charge_unit"`
	NoshowPanelty                             bool      `json:"noshow_penalty" dynamo:"noshow_penalty"`
	NoshowPenalty                             bool      `json:"noshow_panelty" dynamo:"noshow_panelty"`
	NoshowCharge                              float64   `json:"noshow_charge" dynamo:"noshow_charge"`
	Notes                                     MultiLang `json:"notes" dynamo:"notes"`
	NoshowChargeUnit                          string    `json:"noshow_charge_unit" dynamo:"noshow_charge_unit"`
	CancellationChargeAppliedOnServiceTax     bool      `json:"cancellation_charge_applied_on_service_tax" dynamo:"cancellation_charge_applied_on_service_tax"`
	CancellationChargeAppliedOnConsumptionTax bool      `json:"cancellation_charge_applied_on_consumption_tax" dynamo:"cancellation_charge_applied_on_consumption_tax"`
}

type Meal struct {
	Breakfast *bool  `json:"Breakfast,omitempty" dynamo:"Breakfast"`
	Place     string `json:"place" dynamo:"place"`
	Lunch     *bool  `json:"Lunch,omitempty" dynamo:"Lunch"`
	Dinner    *bool  `json:"Dinner,omitempty" dynamo:"Dinner"`
}

type QuestionAnswer struct {
	Type           string    `json:"type" dynamo:"type"`
	QuestionTitle  MultiLang `json:"question_title" dynamo:"question_title"`
	Answer         string    `json:"answer" dynamo:"answer"`
	QuestionID     string    `json:"question_id" dynamo:"question_id"`
	DisplayComment MultiLang `json:"display_comment" dynamo:"display_comment"`
}

// plan
type Plan struct {
	ID                              string                         `json:"id" dynamo:"id"`
	HotelID                         string                         `json:"hotel_id" dynamo:"hotel_id"`
	Title                           MultiLang                      `json:"title" dynamo:"title"`
	Comment                         MultiLang                      `json:"comment" dynamo:"comment"`
	Description                     MultiLang                      `json:"description" dynamo:"description"`
	Code                            string                         `json:"code" dynamo:"code"`
	PmsCode                         string                         `json:"pms_code" dynamo:"pms_code"`
	Type                            string                         `json:"type" dynamo:"type"`
	Status                          string                         `json:"status" dynamo:"status"`
	ConfirmationEmailContent        MultiLang                      `json:"confirmation_email_content" dynamo:"confirmation_email_content"`
	Order                           int64                          `json:"order" dynamo:"order"`
	ShowInPlanList                  bool                           `json:"show_in_plan_list" dynamo:"show_in_plan_list"`
	ShowInTripDotCOM                bool                           `json:"show_in_trip_dot_com" dynamo:"show_in_trip_dot_com"`
	SpecialCodes                    []string                       `json:"special_codes" dynamo:"special_codes"`
	PortalSiteSpecialCodes          []PortalSiteSpecialCodeForPlan `json:"portal_site_special_codes" dynamo:"portal_site_special_codes"`
	ApplyServiceCharge              bool                           `json:"apply_service_charge" dynamo:"apply_service_charge"`
	Payment                         []string                       `json:"payment" dynamo:"payment"`
	RoomIDList                      []string                       `json:"room_id_list" dynamo:"room_id_list"`
	RoomNameList                    []string                       `json:"room_iname_list" dynamo:"room_iname_list"`
	Pricing                         string                         `json:"pricing" dynamo:"pricing"`
	ReservationAvailableCountPerDay int64                          `json:"reservation_available_count_per_day" dynamo:"reservation_available_count_per_day"`
	SalesTimerSwitch                bool                           `json:"sales_timer_switch" dynamo:"sales_timer_switch"`
	SalesStartDate                  string                         `json:"sales_start_date" dynamo:"sales_start_date"`
	SalesEndDate                    string                         `json:"sales_end_date" dynamo:"sales_end_date"`
	SalesAvailableStartDate         string                         `json:"sales_available_start_date" dynamo:"sales_available_start_date"`
	SalesAvailableEndDate           string                         `json:"sales_available_end_date" dynamo:"sales_available_end_date"`
	SalesStartTime                  string                         `json:"sales_start_time" dynamo:"sales_start_time"`
	SalesEndTime                    string                         `json:"sales_end_time" dynamo:"sales_end_time"`
	SalesAvailableStartTime         string                         `json:"sales_available_start_time" dynamo:"sales_available_start_time"`
	SalesAvailableEndTime           string                         `json:"sales_available_end_time" dynamo:"sales_available_end_time"`
	LastCheckInTime                 string                         `json:"last_check_in_time" dynamo:"last_check_in_time"`
	CheckInTime                     string                         `json:"check_in_time" dynamo:"check_in_time"`
	CheckOutTime                    string                         `json:"check_out_time" dynamo:"check_out_time"`
	MinimumNights                   int64                          `json:"minimum_nights" dynamo:"minimum_nights"`
	MaximumNights                   int64                          `json:"maximum_nights" dynamo:"maximum_nights"`
	GenderRestriction               string                         `json:"gender_restriction" dynamo:"gender_restriction"`
	CutOff                          bool                           `json:"cut_off" dynamo:"cut_off"`
	CutOffDays                      int64                          `json:"cut_off_days" dynamo:"cut_off_days"`
	CutOffTime                      string                         `json:"cut_off_time" dynamo:"cut_off_time"`
	ChildRankAdultType              bool                           `json:"child_rank_adult_type" dynamo:"child_rank_adult_type"`
	SalesCategory                   bool                           `json:"sales_category" dynamo:"sales_category"`
	Lunch                           bool                           `json:"lunch" dynamo:"lunch"`
	Dinner                          bool                           `json:"dinner" dynamo:"dinner"`
	Breakfast                       bool                           `json:"breakfast" dynamo:"breakfast"`
	BreakfastPlace                  string                         `json:"Breakfast_place" dynamo:"Breakfast_place"`
	LunchPlace                      string                         `json:"Lunch_place" dynamo:"Lunch_place"`
	DinnerPlace                     string                         `json:"Dinner_place" dynamo:"Dinner_place"`
	ReminderEmailDay                ReminderEmailDay               `json:"reminder_email_day" dynamo:"reminder_email_day"`
	ReminderEmailTime               string                         `json:"reminder_email_time" dynamo:"reminder_email_time"`
	Meal                            []Meal                         `json:"meal" dynamo:"meal"`
	ChildSetting                    ChildSetting                   `json:"child_setting" dynamo:"child_setting"`
	Tax                             []PlanTax                      `json:"tax" dynamo:"tax"`
	PlanImages                      []Image                        `json:"plan_images" dynamo:"plan_images"`
	QuestionsForCustomer            []QuestionsForCustomer         `json:"questions_for_customer" dynamo:"questions_for_customer"`
	RoomInfo                        []RoomInfo                     `json:"room_info" dynamo:"room_info"`
	CancellationPolicy              CancellationPolicy             `json:"cancellation_policy" dynamo:"cancellation_policy"`
	OptionIDList                    []string                       `json:"option_id_list" dynamo:"option_id_list"`
	PlanGroupList                   []PlanGroupList                `json:"plan_group_list" dynamo:"plan_group_list"`
	ApplicableNights                int64                          `json:"applicable_nights" dynamo:"applicable_nights"`
	UseCND                          bool                           `json:"use_cnd" dynamo:"use_cnd"`
	ConsecutiveNightDiscount        CND                            `json:"consecutive_night_discount" dynamo:"consecutive_night_discount"`
	CreatedAt                       int64                          `json:"created_at" dynamo:"created_at"`
	UpdatedAt                       int64                          `json:"updated_at" dynamo:"updated_at"`
}

type CND struct {
	Adult  CNDE `json:"adult" dynamo:"adult"`
	ChildA CNDE `json:"childA" dynamo:"childA"`
	ChildB CNDE `json:"childB" dynamo:"childB"`
	ChildC CNDE `json:"childC" dynamo:"childC"`
	ChildD CNDE `json:"childD" dynamo:"childD"`
	ChildE CNDE `json:"childE" dynamo:"childE"`
	ChildF CNDE `json:"childF" dynamo:"childF"`
}

type CNDE struct {
	Price float64 `json:"price" dynamo:"price"`
	Unit  string  `json:"unit" dynamo:"unit"`
}

type PlanGroupList struct {
	ID   string    `json:"id" dynamo:"id"`
	Name MultiLang `json:"name" dynamo:"name"`
}

type QuestionsForCustomer struct {
	ID             string    `json:"id" dynamo:"id"`
	HotelID        string    `json:"hotel_id" dynamo:"hotel_id"`
	Type           string    `json:"type" dynamo:"type"`
	InputType      string    `json:"input_type" dynamo:"input_type"`
	Title          MultiLang `json:"title" dynamo:"title"`
	DisplayComment MultiLang `json:"display_comment" dynamo:"display_comment"`
	Required       bool      `json:"required" dynamo:"required"`
	Attribute      Attribute `json:"attribute" dynamo:"attribute"`
	CreatedAt      int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt      int64     `json:"updated_at" dynamo:"updated_at"`
}

type Attribute struct {
	SelectableTimeStart *string     `json:"selectable_time_start,omitempty" dynamo:"selectable_time_start"`
	SelectableTimeEnd   *string     `json:"selectable_time_end,omitempty" dynamo:"selectable_time_end"`
	TargetPerson        *string     `json:"target_person,omitempty" dynamo:"target_person"`
	Control             *string     `json:"control,omitempty" dynamo:"control"`
	SelectionMethod     string      `json:"selection_method" dynamo:"selection_method"`
	Choice              []MultiLang `json:"choice" dynamo:"choice"`
}

type ReminderEmailDay struct {
	Email1 int64 `json:"email1" dynamo:"email1"`
	Email2 int64 `json:"email2" dynamo:"email2"`
	Email3 int64 `json:"email3" dynamo:"email3"`
}

type RoomInfo struct {
	RoomID    string    `json:"room_id" dynamo:"room_id"`
	RoomType  MultiLang `json:"room_type" dynamo:"room_type"`
	ListPrice int64     `json:"list_price" dynamo:"list_price"`
}

type PlanTax struct {
	ID           string     `json:"id" dynamo:"id"`
	HotelID      string     `json:"hotel_id" dynamo:"hotel_id"`
	TaxType      string     `json:"tax_type" dynamo:"tax_type"`
	Message      *MultiLang `json:"message,omitempty" dynamo:"message"`
	TaxName      MultiLang  `json:"tax_name" dynamo:"tax_name"`
	IsActive     bool       `json:"is_active" dynamo:"is_active"`
	IsCalculated bool       `json:"is_calculated" dynamo:"is_calculated"`
	URL          *string    `json:"url,omitempty" dynamo:"url"`
	CreatedAt    int64      `json:"created_at" dynamo:"created_at"`
	UpdatedAt    int64      `json:"updated_at" dynamo:"updated_at"`
	Period1      *Period    `json:"period1,omitempty" dynamo:"period1"`
	Period2      *Period    `json:"period2,omitempty" dynamo:"period2"`
	AppliedOn    *string    `json:"applied_on,omitempty" dynamo:"applied_on"`
}

type Tax []TaxItem

type TaxItem struct {
	ID                           string     `json:"id" dynamo:"id"`
	HotelID                      string     `json:"hotel_id" dynamo:"hotel_id"`
	TaxType                      string     `json:"tax_type" dynamo:"tax_type"`
	Message                      *MultiLang `json:"message,omitempty" dynamo:"message"`
	TaxName                      MultiLang  `json:"tax_name" dynamo:"tax_name"`
	IsActive                     bool       `json:"is_active" dynamo:"is_active"`
	IsCalculated                 bool       `json:"is_calculated" dynamo:"is_calculated"`
	URL                          *string    `json:"url,omitempty" dynamo:"url"`
	CreatedAt                    int64      `json:"created_at" dynamo:"created_at"`
	UpdatedAt                    int64      `json:"updated_at" dynamo:"updated_at"`
	Period1                      *Period    `json:"period1,omitempty" dynamo:"period1"`
	Period2                      *Period    `json:"period2,omitempty" dynamo:"period2"`
	AppliedOn                    *string    `json:"applied_on,omitempty" dynamo:"applied_on"`
	ServiceTaxRoundingMethod     string     `json:"service_tax_rounding_method,omitempty" dynamo:"service_tax_rounding_method"`
	ConsumptionTaxRoundingMethod string     `json:"consumption_tax_rounding_method,omitempty" dynamo:"consumption_tax_rounding_method"`
}

type RateRank struct {
	ID          string `json:"id" dynamo:"id"`
	HotelID     string `json:"hotel_id" dynamo:"hotel_id"`
	Name        string `json:"name" dynamo:"name"`
	Description string `json:"description" dynamo:"description"`
	Colour      string `json:"colour" dynamo:"colour"`
	Order       int64  `json:"order" dynamo:"order"`
	CreatedAt   int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64  `json:"updated_at" dynamo:"updated_at"`
}

type PlanGroup struct {
	ID                  string    `json:"id" dynamo:"id"`
	HotelID             string    `json:"hotel_id" dynamo:"hotel_id"`
	Name                MultiLang `json:"name" dynamo:"name"`
	Description         string    `json:"description" dynamo:"description"`
	WithSpecialDiscount bool      `json:"with_special_discount" dynamo:"with_special_discount"`
	PartialDiscount     bool      `json:"partial_discount" dynamo:"partial_discount"`
	Unit                string    `json:"unit" dynamo:"unit"`
	Discount            float64   `json:"discount" dynamo:"discount"`
	DiscountName        MultiLang `json:"discount_name" dynamo:"discount_name"`
	Order               int64     `json:"order" dynamo:"order"`
	BookingStartDate    string    `json:"booking_start_date" dynamo:"booking_start_date"`
	BookingEndDate      string    `json:"booking_end_date" dynamo:"booking_end_date"`
	CheckinStartDate    string    `json:"checkin_start_date" dynamo:"checkin_start_date"`
	CheckinEndDate      string    `json:"checkin_end_date" dynamo:"checkin_end_date"`
	DiscountAmountLimit float64   `json:"discount_amount_limit" dynamo:"discount_amount_limit"`
	ExclusionStartDate  string    `json:"exclusion_start_date" dynamo:"exclusion_start_date"`
	ExclusionEndDate    string    `json:"exclusion_end_date" dynamo:"exclusion_end_date"`
	CreatedAt           int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt           int64     `json:"updated_at" dynamo:"updated_at"`
}

type RoomCategory struct {
	ID          string    `json:"id" dynamo:"id"`
	HotelID     string    `json:"hotel_id" dynamo:"hotel_id"`
	Name        MultiLang `json:"name" dynamo:"name"`
	Description string    `json:"description" dynamo:"description"`
	Order       int       `json:"order" dynamo:"order"`
	CreatedAt   int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64     `json:"updated_at" dynamo:"updated_at"`
}

type SpecialCode struct {
	ID          string `json:"id" dynamo:"id"`
	HotelID     string `json:"hotel_id" dynamo:"hotel_id"`
	Code        string `json:"code" dynamo:"code"`
	Order       int64  `json:"order" dynamo:"order"`
	Description string `json:"description" dynamo:"description"`
	StartDate   string `json:"start_date" dynamo:"start_date"`
	EndDate     string `json:"end_date" dynamo:"end_date"`
	CreatedAt   int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64  `json:"updated_at" dynamo:"updated_at"`
}

type EmailTemplate struct {
	ID        string `json:"id" dynamo:"id"`
	HotelID   string `json:"hotel_id" dynamo:"hotel_id"`
	Name      string `json:"name" dynamo:"name"`
	Desc      string `json:"desc" dynamo:"desc"`
	Language  string `json:"language" dynamo:"language"`
	Type      string `json:"type" dynamo:"type"`
	Title     string `json:"title" dynamo:"title"`
	Body      string `json:"body" dynamo:"body"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type EmailSetting struct {
	ID             string   `json:"id" dynamo:"id"`
	HotelID        string   `json:"hotel_id" dynamo:"hotel_id"`
	EmailType      string   `json:"email_type" dynamo:"email_type"`
	EmailSendDate  int64    `json:"email_send_date" dynamo:"email_send_date"`
	EmailSendTime  string   `json:"email_send_time" dynamo:"email_send_time"`
	EmailBcc       string   `json:"email_bcc" dynamo:"email_bcc"`
	Destination    []string `json:"destination" dynamo:"destination"`
	TemplateJp     string   `json:"template_jp" dynamo:"template_jp"`
	TemplateJpName string   `json:"template_jp_name" dynamo:"template_jp_name"`
	TemplateJpDesc string   `json:"template_jp_desc" dynamo:"template_jp_desc"`
	TemplateEn     string   `json:"template_en" dynamo:"template_en"`
	TemplateEnName string   `json:"template_en_name" dynamo:"template_en_name"`
	TemplateEnDesc string   `json:"template_en_desc" dynamo:"template_en_desc"`
	CreatedAt      int64    `json:"created_at" dynamo:"created_at"`
	UpdatedAt      int64    `json:"updated_at" dynamo:"updated_at"`
}

type Gallery struct {
	ID                 string   `json:"id" dynamo:"id"`
	HotelID            string   `json:"hotel_id" dynamo:"hotel_id"`
	Type               string   `json:"type" dynamo:"type"`
	Image              string   `json:"image" dynamo:"image"`
	ImageName          string   `json:"image_name" dynamo:"image_name"`
	URL                string   `json:"url" dynamo:"url"`
	Alt                string   `json:"alt" dynamo:"alt"`
	Desc               string   `json:"desc" dynamo:"desc"`
	IsHide             bool     `json:"is_hide" dynamo:"is_hide"`
	UseCount           int64    `json:"use_count" dynamo:"use_count"`
	ShareCount         int64    `json:"share_count" dynamo:"share_count"`
	SharedIdList       []string `json:"shared_id_list" dynamo:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category" dynamo:"shared_with_category"`
	CreatedAt          int64    `json:"created_at" dynamo:"created_at"`
	UpdatedAt          int64    `json:"updated_at" dynamo:"updated_at"`
}

type SiteControllerUser struct {
	HotelID            string `json:"hotel_id" dynamo:"hotel_id"`
	LoginPass          string `json:"LoginPass" dynamo:"LoginPass"`
	SiteControllerName string `json:"site_controller_name" dynamo:"site_controller_name"`
	LoginID            string `json:"LoginID" dynamo:"LoginID"`
	CreatedAt          int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt          int64  `json:"updated_at" dynamo:"updated_at"`
}

type User struct {
	HotelID      string `json:"hotel_id" dynamo:"hotel_id"`
	FacilityCode string `json:"facility_code" dynamo:"facility_code"`
	Password     string `json:"password" dynamo:"password"`
	LastLogin    string `json:"last_login" dynamo:"last_login"`
	UserName     string `json:"user_name" dynamo:"user_name"`
	UserSub      string `json:"user_sub" dynamo:"user_sub"`
}

type PortalSiteUser struct {
	PortalSiteID string `json:"portal_site_id" dynamo:"portal_site_id"`
	Password     string `json:"password" dynamo:"password"`
	UserName     string `json:"user_name" dynamo:"user_name"`
	UserSub      string `json:"user_sub" dynamo:"user_sub"`
	CreatedAt    string `json:"created_at" dynamo:"created_at"`
	UpdatedAt    string `json:"updated_at" dynamo:"updated_at"`
}

type NG struct {
	Response string `json:"response" dynamo:"response"`
	Error    string `json:"error" dynamo:"error"`
}

type OK struct {
	Response string `json:"response" dynamo:"response"`
}

type CPAChangeInfo struct {
	HotelID                    string         `json:"hotel_id" dynamo:"hotel_id"`
	GoogleHotelAdsCPA          float64        `json:"google_hotel_ads_cpa" dynamo:"google_hotel_ads_cpa"`
	GoogleHotelAdsCPARequested float64        `json:"google_hotel_ads_cpa_requested" dynamo:"google_hotel_ads_cpa_requested"`
	CPAChangeRequestDate       string         `json:"cpa_change_request_date" dynamo:"cpa_change_request_date"`
	CPAChangeRequestCompleted  bool           `json:"cpa_change_request_completed" dynamo:"cpa_change_request_completed"`
	CPAChangeLog               []CPAChangeLog `json:"cpa_change_log" dynamo:"cpa_change_log"`
	UseGoogleHotelAds          bool           `json:"use_google_hotel_ads" dynamo:"use_google_hotel_ads"`
	CreatedAt                  int64          `json:"created_at" dynamo:"created_at"`
	UpdatedAt                  int64          `json:"updated_at" dynamo:"updated_at"`
}

type CPAChangeLog struct {
	RequestDate  string  `json:"request_date" dynamo:"request_date"`
	CompleteDate string  `json:"complete_date" dynamo:"complete_date"`
	OldCPA       float64 `json:"old_cpa" dynamo:"old_cpa"`
	NewCPA       float64 `json:"new_cpa" dynamo:"new_cpa"`
}

type PlanPrice struct {
	UseDate                          string                         `json:"use_date" dynamo:"use_date"`
	ID                               string                         `json:"id" dynamo:"id"`
	HotelID                          string                         `json:"hotel_id" dynamo:"hotel_id"`
	PlanID                           string                         `json:"plan_id" dynamo:"plan_id"`
	PlanCode                         string                         `json:"plan_code" dynamo:"plan_code"`
	PlanRoomUseDatePax               string                         `json:"plan_room_use_date_pax" dynamo:"plan_room_use_date_pax"`
	Title                            MultiLang                      `json:"title" dynamo:"title"`
	Comment                          MultiLang                      `json:"comment" dynamo:"comment"`
	Description                      MultiLang                      `json:"description" dynamo:"description"`
	PlanType                         string                         `json:"plan_type" dynamo:"plan_type"`
	Order                            int64                          `json:"order" dynamo:"order"`
	RoomOrder                        int64                          `json:"room_order" dynamo:"room_order"`
	ShowInPlanList                   bool                           `json:"show_in_plan_list" dynamo:"show_in_plan_list"`
	SpecialCodes                     []string                       `json:"special_codes" dynamo:"special_codes"`
	PortalSiteSpecialCodes           []PortalSiteSpecialCodeForPlan `json:"portal_site_special_codes" dynamo:"portal_site_special_codes"`
	Breakfast                        bool                           `json:"Breakfast" dynamo:"Breakfast"`
	BreakfastPlace                   string                         `json:"Breakfast_place" dynamo:"Breakfast_place"`
	Lunch                            bool                           `json:"Lunch" dynamo:"Lunch"`
	LunchPlace                       string                         `json:"Lunch_place" dynamo:"Lunch_place"`
	Dinner                           bool                           `json:"Dinner" dynamo:"Dinner"`
	DinnerPlace                      string                         `json:"Dinner_place" dynamo:"Dinner_place"`
	Cancellable                      bool                           `json:"cancellable" dynamo:"cancellable"`
	CancellationPolicy               CancellationPolicy             `json:"cancellation_policy" dynamo:"cancellation_policy"`
	Payment                          []string                       `json:"payment" dynamo:"payment"`
	Pricing                          string                         `json:"pricing" dynamo:"pricing"`
	SalesTimerSwitch                 bool                           `json:"sales_timer_switch" dynamo:"sales_timer_switch"`
	SalesAvailableStartDate          string                         `json:"sales_available_start_date" dynamo:"sales_available_start_date"`
	SalesAvailableEndDate            string                         `json:"sales_available_end_date" dynamo:"sales_available_end_date"`
	SalesStartTime                   string                         `json:"sales_start_time" dynamo:"sales_start_time"`
	SalesEndTime                     string                         `json:"sales_end_time" dynamo:"sales_end_time"`
	SalesAvailableStartTime          string                         `json:"sales_available_start_time" dynamo:"sales_available_start_time"`
	SalesAvailableEndTime            string                         `json:"sales_available_end_time" dynamo:"sales_available_end_time"`
	ReservationAvailableCountPerDay  int64                          `json:"reservation_available_count_per_day" dynamo:"reservation_available_count_per_day"`
	ReservationAvailableCurrentCount int64                          `json:"reservation_available_current_count" dynamo:"reservation_available_current_count"`
	LastCheckInTime                  string                         `json:"last_check_in_time" dynamo:"last_check_in_time"`
	CheckInTime                      string                         `json:"check_in_time" dynamo:"check_in_time"`
	CheckOutTime                     string                         `json:"check_out_time" dynamo:"check_out_time"`
	MinimumNights                    int64                          `json:"minimum_nights" dynamo:"minimum_nights"`
	MaximumNights                    int64                          `json:"maximum_nights" dynamo:"maximum_nights"`
	ChildRankAdultType               bool                           `json:"child_rank_adult_type" dynamo:"child_rank_adult_type"`
	ChildADesc                       MultiLang                      `json:"child_A_desc" dynamo:"child_A_desc"`
	ChildAIncludeInCapacity          bool                           `json:"child_A_include_in_capacity" dynamo:"child_A_include_in_capacity"`
	ChildASetting                    ChildInfo                      `json:"child_A_setting" dynamo:"child_A_setting"`
	ChildBDesc                       MultiLang                      `json:"child_B_desc" dynamo:"child_B_desc"`
	ChildBIncludeInCapacity          bool                           `json:"child_B_include_in_capacity" dynamo:"child_B_include_in_capacity"`
	ChildBSetting                    ChildInfo                      `json:"child_B_setting" dynamo:"child_B_setting"`
	ChildCDesc                       MultiLang                      `json:"child_C_desc" dynamo:"child_C_desc"`
	ChildCIncludeInCapacity          bool                           `json:"child_C_include_in_capacity" dynamo:"child_C_include_in_capacity"`
	ChildCSetting                    ChildInfo                      `json:"child_C_setting" dynamo:"child_C_setting"`
	ChildDDesc                       MultiLang                      `json:"child_D_desc" dynamo:"child_D_desc"`
	ChildDIncludeInCapacity          bool                           `json:"child_D_include_in_capacity" dynamo:"child_D_include_in_capacity"`
	ChildDSetting                    ChildInfo                      `json:"child_D_setting" dynamo:"child_D_setting"`
	ChildEDesc                       MultiLang                      `json:"child_E_desc" dynamo:"child_E_desc"`
	ChildEIncludeInCapacity          bool                           `json:"child_E_include_in_capacity" dynamo:"child_E_include_in_capacity"`
	ChildESetting                    ChildInfo                      `json:"child_E_setting" dynamo:"child_E_setting"`
	ChildFDesc                       MultiLang                      `json:"child_F_desc" dynamo:"child_F_desc"`
	ChildFIncludeInCapacity          bool                           `json:"child_F_include_in_capacity" dynamo:"child_F_include_in_capacity"`
	ChildFSetting                    ChildInfo                      `json:"child_F_setting" dynamo:"child_F_setting"`
	GenderRestriction                string                         `json:"gender_restriction" dynamo:"gender_restriction"`
	Status                           string                         `json:"status" dynamo:"status"`
	CutOff                           bool                           `json:"cut_off" dynamo:"cut_off"`
	CutOffDays                       int64                          `json:"cut_off_days" dynamo:"cut_off_days"`
	CutOffTime                       string                         `json:"cut_off_time" dynamo:"cut_off_time"`
	Tax                              []TaxItem                      `json:"tax" dynamo:"tax"`
	PlanImages                       []Image                        `json:"plan_images" dynamo:"plan_images"`
	RoomID                           string                         `json:"room_id" dynamo:"room_id"`
	RoomStatus                       string                         `json:"room_status" dynamo:"room_status"`
	RoomType                         MultiLang                      `json:"room_type" dynamo:"room_type"`
	RoomDesc                         MultiLang                      `json:"room_desc" dynamo:"room_desc"`
	CountTotal                       int64                          `json:"count_total" dynamo:"count_total"`
	CountReserved                    int64                          `json:"count_reserved" dynamo:"count_reserved"`
	CountVacant                      int64                          `json:"count_vacant" dynamo:"count_vacant"`
	RoomSize                         float64                        `json:"room_size" dynamo:"room_size"`
	IsSmoking                        bool                           `json:"is_smoking" dynamo:"is_smoking"`
	SmokeComment                     MultiLang                      `json:"smoke_comment" dynamo:"smoke_comment"`
	MaxPax                           int64                          `json:"max_pax" dynamo:"max_pax"`
	MaxAdult                         int64                          `json:"max_adult" dynamo:"max_adult"`
	MaxChildren                      int64                          `json:"max_children" dynamo:"max_children"`
	MinOccupancy                     int64                          `json:"min_occupancy" dynamo:"min_occupancy"`
	BedCount                         int64                          `json:"bed_count" dynamo:"bed_count"`
	Facilities                       []MultiLang                    `json:"facilities" dynamo:"facilities"`
	BathroomAminities                []MultiLang                    `json:"bathroom_aminities" dynamo:"bathroom_aminities"`
	ListPrice                        int64                          `json:"list_price" dynamo:"list_price"`
	RoomImages                       []Image                        `json:"room_images" dynamo:"room_images"`
	Pax                              int64                          `json:"pax" dynamo:"pax"`
	PaxList                          [][]int64                      `json:"pax_list" dynamo:"pax_list"`
	RateRankID                       string                         `json:"rate_rank_id" dynamo:"rate_rank_id"`
	SalesCategory                    bool                           `json:"sales_category" dynamo:"sales_category"`
	Price                            int64                          `json:"price" dynamo:"price"`
	ConsumptionCharge                int64                          `json:"consumption_charge" dynamo:"consumption_charge"`
	ServiceCharge                    int64                          `json:"service_charge" dynamo:"service_charge"`
	PriceList                        [][]int64                      `json:"price_list" dynamo:"price_list"`
	SCPrice                          int64                          `json:"sc_price" dynamo:"sc_price"`
	TotalPriceForSorting             int64                          `json:"total_price_for_sorting" dynamo:"total_price_for_sorting"`
	QuestionsForCustomer             []QuestionsForCustomer         `json:"questions_for_customer" dynamo:"questions_for_customer"`
	Beddings                         []Bedding                      `json:"beddings" dynamo:"beddings"`
	ApplyServiceCharge               bool                           `json:"apply_service_charge" dynamo:"apply_service_charge"`
	ChildAPrice                      int64                          `json:"child_A_price" dynamo:"child_A_price"`
	ChildBPrice                      int64                          `json:"child_B_price" dynamo:"child_B_price"`
	ChildCPrice                      int64                          `json:"child_C_price" dynamo:"child_C_price"`
	ChildDPrice                      int64                          `json:"child_D_price" dynamo:"child_D_price"`
	ChildEPrice                      int64                          `json:"child_E_price" dynamo:"child_E_price"`
	ChildFPrice                      int64                          `json:"child_F_price" dynamo:"child_F_price"`
	ChildAListPrice                  [][]int64                      `json:"child_A_list_price" dynamo:"child_A_list_price"`
	ChildBListPrice                  [][]int64                      `json:"child_B_list_price" dynamo:"child_B_list_price"`
	ChildCListPrice                  [][]int64                      `json:"child_C_list_price" dynamo:"child_C_list_price"`
	ChildDListPrice                  [][]int64                      `json:"child_D_list_price" dynamo:"child_D_list_price"`
	ChildEListPrice                  [][]int64                      `json:"child_E_list_price" dynamo:"child_E_list_price"`
	ChildFListPrice                  [][]int64                      `json:"child_F_list_price" dynamo:"child_F_list_price"`
	ChildAServiceTaxList             [][]int64                      `json:"child_A_service_tax_list" dynamo:"child_A_service_tax_list"`
	ChildAConsumptionTaxList         [][]int64                      `json:"child_A_consumption_tax_list" dynamo:"child_A_consumption_tax_list"`
	ChildBServiceTaxList             [][]int64                      `json:"child_B_service_tax_list" dynamo:"child_B_service_tax_list"`
	ChildBConsumptionTaxList         [][]int64                      `json:"child_B_consumption_tax_list" dynamo:"child_B_consumption_tax_list"`
	ChildCServiceTaxList             [][]int64                      `json:"child_C_service_tax_list" dynamo:"child_C_service_tax_list"`
	ChildCConsumptionTaxList         [][]int64                      `json:"child_C_consumption_tax_list" dynamo:"child_C_consumption_tax_list"`
	ChildDServiceTaxList             [][]int64                      `json:"child_D_service_tax_list" dynamo:"child_D_service_tax_list"`
	ChildDConsumptionTaxList         [][]int64                      `json:"child_D_consumption_tax_list" dynamo:"child_D_consumption_tax_list"`
	ChildEServiceTaxList             [][]int64                      `json:"child_E_service_tax_list" dynamo:"child_E_service_tax_list"`
	ChildEConsumptionTaxList         [][]int64                      `json:"child_E_consumption_tax_list" dynamo:"child_E_consumption_tax_list"`
	ChildFServiceTaxList             [][]int64                      `json:"child_F_service_tax_list" dynamo:"child_F_service_tax_list"`
	ChildFConsumptionTaxList         [][]int64                      `json:"child_F_consumption_tax_list" dynamo:"child_F_consumption_tax_list"`
	ServiceTaxList                   [][]int64                      `json:"service_tax_list" dynamo:"service_tax_list"`
	ConsumptionTaxList               [][]int64                      `json:"consumption_tax_list" dynamo:"consumption_tax_list"`
	ChildAServiceTax                 int64                          `json:"child_A_service_tax" dynamo:"child_A_service_tax"`
	ChildAConsumptionTax             int64                          `json:"child_A_consumption_tax" dynamo:"child_A_consumption_tax"`
	ChildBServiceTax                 int64                          `json:"child_B_service_tax" dynamo:"child_B_service_tax"`
	ChildBConsumptionTax             int64                          `json:"child_B_consumption_tax" dynamo:"child_B_consumption_tax"`
	ChildCServiceTax                 int64                          `json:"child_C_service_tax" dynamo:"child_C_service_tax"`
	ChildCConsumptionTax             int64                          `json:"child_C_consumption_tax" dynamo:"child_C_consumption_tax"`
	ChildDServiceTax                 int64                          `json:"child_D_service_tax" dynamo:"child_D_service_tax"`
	ChildDConsumptionTax             int64                          `json:"child_D_consumption_tax" dynamo:"child_D_consumption_tax"`
	ChildEServiceTax                 int64                          `json:"child_E_service_tax" dynamo:"child_E_service_tax"`
	ChildEConsumptionTax             int64                          `json:"child_E_consumption_tax" dynamo:"child_E_consumption_tax"`
	ChildFServiceTax                 int64                          `json:"child_F_service_tax" dynamo:"child_F_service_tax"`
	ChildFConsumptionTax             int64                          `json:"child_F_consumption_tax" dynamo:"child_F_consumption_tax"`
	PlanPriceIDList                  [][]string                     `json:"plan_price_id_list" dynamo:"plan_price_id_list"`
	ApplicableNights                 int64                          `json:"applicable_nights" dynamo:"applicable_nights"`
	UseCND                           bool                           `json:"use_cnd" dynamo:"use_cnd"`
	ConsecutiveNightDiscount         CND                            `json:"consecutive_night_discount" dynamo:"consecutive_night_discount"`
	CreatedAt                        int64                          `json:"created_at" dynamo:"created_at"`
	UpdatedAt                        int64                          `json:"updated_at" dynamo:"updated_at"`
	CNDPrice                         int64                          `json:"cnd_price" dynamo:"cnd_price"`                     //price after removing cnd discount
	CNDPriceAfterTax                 int64                          `json:"cnd_price_after_tax" dynamo:"cnd_price_after_tax"` //price after removing tax from CNDPrice
	CNDChildAPrice                   int64                          `json:"cnd_child_A_price" dynamo:"cnd_child_A_price"`
	CNDChildBPrice                   int64                          `json:"cnd_child_B_price" dynamo:"cnd_child_B_price"`
	CNDChildCPrice                   int64                          `json:"cnd_child_C_price" dynamo:"cnd_child_C_price"`
	CNDChildDPrice                   int64                          `json:"cnd_child_D_price" dynamo:"cnd_child_D_price"`
	CNDChildEPrice                   int64                          `json:"cnd_child_E_price" dynamo:"cnd_child_E_price"`
	CNDChildFPrice                   int64                          `json:"cnd_child_F_price" dynamo:"cnd_child_F_price"`
	CNDConsumptionCharge             int64                          `json:"cnd_consumption_charge" dynamo:"cnd_consumption_charge"`
	CNDServiceCharge                 int64                          `json:"cnd_service_charge" dynamo:"cnd_service_charge"`
	CNDPriceList                     [][]int64                      `json:"cnd_price_list" dynamo:"cnd_price_list"` // collection of all cnd_price_after_tax
	CNDChildAListPrice               [][]int64                      `json:"cnd_child_A_list_price" dynamo:"cnd_child_A_list_price"`
	CNDChildBListPrice               [][]int64                      `json:"cnd_child_B_list_price" dynamo:"cnd_child_B_list_price"`
	CNDChildCListPrice               [][]int64                      `json:"cnd_child_C_list_price" dynamo:"cnd_child_C_list_price"`
	CNDChildDListPrice               [][]int64                      `json:"cnd_child_D_list_price" dynamo:"cnd_child_D_list_price"`
	CNDChildEListPrice               [][]int64                      `json:"cnd_child_E_list_price" dynamo:"cnd_child_E_list_price"`
	CNDChildFListPrice               [][]int64                      `json:"cnd_child_F_list_price" dynamo:"cnd_child_F_list_price"`
	CNDChildAServiceTaxList          [][]int64                      `json:"cnd_child_A_service_tax_list" dynamo:"cnd_child_A_service_tax_list"`
	CNDChildAConsumptionTaxList      [][]int64                      `json:"cnd_child_A_consumption_tax_list" dynamo:"cnd_child_A_consumption_tax_list"`
	CNDChildBServiceTaxList          [][]int64                      `json:"cnd_child_B_service_tax_list" dynamo:"cnd_child_B_service_tax_list"`
	CNDChildBConsumptionTaxList      [][]int64                      `json:"cnd_child_B_consumption_tax_list" dynamo:"cnd_child_B_consumption_tax_list"`
	CNDChildCServiceTaxList          [][]int64                      `json:"cnd_child_C_service_tax_list" dynamo:"cnd_child_C_service_tax_list"`
	CNDChildCConsumptionTaxList      [][]int64                      `json:"cnd_child_C_consumption_tax_list" dynamo:"cnd_child_C_consumption_tax_list"`
	CNDChildDServiceTaxList          [][]int64                      `json:"cnd_child_D_service_tax_list" dynamo:"cnd_child_D_service_tax_list"`
	CNDChildDConsumptionTaxList      [][]int64                      `json:"cnd_child_D_consumption_tax_list" dynamo:"cnd_child_D_consumption_tax_list"`
	CNDChildEServiceTaxList          [][]int64                      `json:"cnd_child_E_service_tax_list" dynamo:"cnd_child_E_service_tax_list"`
	CNDChildEConsumptionTaxList      [][]int64                      `json:"cnd_child_E_consumption_tax_list" dynamo:"cnd_child_E_consumption_tax_list"`
	CNDChildFServiceTaxList          [][]int64                      `json:"cnd_child_F_service_tax_list" dynamo:"cnd_child_F_service_tax_list"`
	CNDChildFConsumptionTaxList      [][]int64                      `json:"cnd_child_F_consumption_tax_list" dynamo:"cnd_child_F_consumption_tax_list"`
	CNDServiceTaxList                [][]int64                      `json:"cnd_service_tax_list" dynamo:"cnd_service_tax_list"`
	CNDConsumptionTaxList            [][]int64                      `json:"cnd_consumption_tax_list" dynamo:"cnd_consumption_tax_list"`
	CNDChildAServiceTax              int64                          `json:"cnd_child_A_service_tax" dynamo:"cnd_child_A_service_tax"`
	CNDChildAConsumptionTax          int64                          `json:"cnd_child_A_consumption_tax" dynamo:"cnd_child_A_consumption_tax"`
	CNDChildBServiceTax              int64                          `json:"cnd_child_B_service_tax" dynamo:"cnd_child_B_service_tax"`
	CNDChildBConsumptionTax          int64                          `json:"cnd_child_B_consumption_tax" dynamo:"cnd_child_B_consumption_tax"`
	CNDChildCServiceTax              int64                          `json:"cnd_child_C_service_tax" dynamo:"cnd_child_C_service_tax"`
	CNDChildCConsumptionTax          int64                          `json:"cnd_child_C_consumption_tax" dynamo:"cnd_child_C_consumption_tax"`
	CNDChildDServiceTax              int64                          `json:"cnd_child_D_service_tax" dynamo:"cnd_child_D_service_tax"`
	CNDChildDConsumptionTax          int64                          `json:"cnd_child_D_consumption_tax" dynamo:"cnd_child_D_consumption_tax"`
	CNDChildEServiceTax              int64                          `json:"cnd_child_E_service_tax" dynamo:"cnd_child_E_service_tax"`
	CNDChildEConsumptionTax          int64                          `json:"cnd_child_E_consumption_tax" dynamo:"cnd_child_E_consumption_tax"`
	CNDChildFServiceTax              int64                          `json:"cnd_child_F_service_tax" dynamo:"cnd_child_F_service_tax"`
	CNDChildFConsumptionTax          int64                          `json:"cnd_child_F_consumption_tax" dynamo:"cnd_child_F_consumption_tax"`
	RoomCategoryList                 []RoomCategoryList             `json:"room_category_list" dynamo:"room_category_list"`
	PlanGroupList                    []PlanGroupList                `json:"plan_group_list" dynamo:"plan_group_list"`
	OptionIDList                     []string                       `json:"option_id_list" dynamo:"option_id_list"`
	PortalSiteSpecialCodeFlg         bool                           `json:"portal_site_special_code_flg"`
}

type ResChanges struct {
	HotelID            string          `json:"hotel_id" dynamo:"hotel_id"`
	PlanID             string          `json:"plan_id" dynamo:"plan_id"`
	AccomodationCharge string          `json:"accomodation_charge" dynamo:"accomodation_charge"`
	ServiceCharge      string          `json:"service_charge" dynamo:"service_charge"`
	ConsumptionCharge  string          `json:"consumption_charge" dynamo:"consumption_charge"`
	TotalCharge        string          `json:"total_charge" dynamo:"total_charge"`
	OptionsCharge      []OptionsCharge `json:"options_charge" dynamo:"options_charge"`
}

type OptionsCharge struct {
	Name     string `json:"name" dynamo:"name"`
	Quantity int64  `json:"quantity" dynamo:"quantity"`
	Price    int64  `json:"price" dynamo:"price"`
}

type Price struct {
	UseDate             string    `json:"use_date" dynamo:"use_date"`
	HotelID             string    `json:"hotel_id" dynamo:"hotel_id"`
	PlanRoomUseDatePax  string    `json:"plan_room_use_date_pax" dynamo:"plan_room_use_date_pax"`
	PlanTitle           MultiLang `json:"plan_title" dynamo:"plan_title"`
	RoomID              string    `json:"room_id" dynamo:"room_id"`
	RoomType            MultiLang `json:"room_type" dynamo:"room_type"`
	RateRankID          string    `json:"rate_rank_id" dynamo:"rate_rank_id"`
	RateRankName        string    `json:"rate_rank_name" dynamo:"rate_rank_name"`
	SalesCategory       bool      `json:"sales_category" dynamo:"sales_category"`
	Pax                 int64     `json:"pax" dynamo:"pax"`
	Price               float64   `json:"price" dynamo:"price"`
	IsBooked            bool      `json:"is_booked" dynamo:"is_booked"`
	ChargeSystem        string    `json:"charge_system" dynamo:"charge_system"`
	TLRoomPricingSystem string    `json:"tl_room_pricing_system" dynamo:"tl_room_pricing_system"` // possible values Per stay/ Per night
	CreatedAt           int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt           int64     `json:"updated_at" dynamo:"updated_at"`
}

type PriceIndex struct {
	UseDate             string    `json:"use_date" dynamo:"use_date"`
	HotelID             string    `json:"hotel_id" dynamo:"hotel_id"`
	PlanRoomUseDatePax  string    `json:"plan_room_use_date_pax" dynamo:"plan_room_use_date_pax"`
	PlanTitle           MultiLang `json:"plan_title" dynamo:"plan_title"`
	RoomID              string    `json:"room_id" dynamo:"room_id"`
	RoomType            MultiLang `json:"room_type" dynamo:"room_type"`
	RateRankID          string    `json:"rate_rank_id" dynamo:"rate_rank_id"`
	RateRankName        string    `json:"rate_rank_name" dynamo:"rate_rank_name"`
	SalesCategory       bool      `json:"sales_category" dynamo:"sales_category"`
	Pax                 int64     `json:"pax" dynamo:"pax"`
	Price               float64   `json:"price" dynamo:"price"`
	IsBooked            bool      `json:"is_booked" dynamo:"is_booked"`
	IsPriceSet          bool      `json:"is_price_set" dynamo:"is_price_set"`
	ChargeSystem        string    `json:"charge_system" dynamo:"charge_system"`
	TLRoomPricingSystem string    `json:"tl_room_pricing_system" dynamo:"tl_room_pricing_system"` // possible values Per stay/ Per night
	IsSpecialRankPrice  bool      `json:"is_special_rank_price" dynamo:"is_special_rank_price"`   //either price is changed (+/-) or it is stopped. BG color is changed.
	CreatedAt           int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt           int64     `json:"updated_at" dynamo:"updated_at"`
}

type Inventory struct {
	HotelID            string    `json:"hotel_id" dynamo:"hotel_id"`
	HotelRoomIDUseDate string    `json:"hotel_room_id_use_date" dynamo:"hotel_room_id_use_date"`
	RoomType           MultiLang `json:"room_type" dynamo:"room_type"`
	UseDate            string    `json:"use_date" dynamo:"use_date"`
	ReserveCount       int64     `json:"reserve_count" dynamo:"reserve_count"`
	VacentCount        int64     `json:"vacent_count" dynamo:"vacent_count"`
	SalesCategory      bool      `json:"sales_category" dynamo:"sales_category"`
	CreatedAt          int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt          int64     `json:"updated_at" dynamo:"updated_at"`
}

type InventoryIndex struct {
	HotelID            string    `json:"hotel_id" dynamo:"hotel_id"`
	HotelRoomIDUseDate string    `json:"hotel_room_id_use_date" dynamo:"hotel_room_id_use_date"`
	RoomType           MultiLang `json:"room_type" dynamo:"room_type"`
	UseDate            string    `json:"use_date" dynamo:"use_date"`
	ReserveCount       int64     `json:"reserve_count" dynamo:"reserve_count"`
	VacentCount        int64     `json:"vacent_count" dynamo:"vacent_count"`
	SalesCategory      bool      `json:"sales_category" dynamo:"sales_category"`
	IsInvSet           bool      `json:"is_inv_set" dynamo:"is_inv_set"`
	CreatedAt          int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt          int64     `json:"updated_at" dynamo:"updated_at"`
}

type RCBookingForm struct {
	ID                            string    `json:"id" dynamo:"id"`
	GroupHotelId                  string    `json:"group_hotel_id" dynamo:"group_hotel_id"`
	Resistered                    bool      `json:"resistered" dynamo:"resistered"`
	HotelName                     MultiLang `json:"hotel_name" dynamo:"hotel_name"`
	CompanyName                   string    `json:"company_name" dynamo:"company_name"`
	CompanyNameKana               string    `json:"company_name_kana" dynamo:"company_name_kana"`
	RepresentativeName            string    `json:"representative_name" dynamo:"representative_name"`
	RepresentativeNameKana        string    `json:"representative_name_kana" dynamo:"representative_name_kana"`
	RepresentativeTitle           string    `json:"representative_title" dynamo:"representative_title"`
	ContactTel                    string    `json:"contact_tel" dynamo:"contact_tel"`
	Zipcode                       string    `json:"zipcode" dynamo:"zipcode"`
	Prefecture                    string    `json:"prefecture" dynamo:"prefecture"`
	Address1                      string    `json:"address_1" dynamo:"address_1"`
	Address2                      string    `json:"address_2" dynamo:"address_2"`
	PrefectureOfPermissionNumber  string    `json:"prefecture_of_permission_number" dynamo:"prefecture_of_permission_number"`
	AreaOfPermissionNumber        string    `json:"area_of_permission_number" dynamo:"area_of_permission_number"`
	PermissionNumber              string    `json:"permission_number" dynamo:"permission_number"`
	ContractorType                string    `json:"contractor_type" dynamo:"contractor_type"`
	GHAZipcode                    string    `json:"gha_zipcode" dynamo:"gha_zipcode"`
	GHAPrefecture                 MultiLang `json:"gha_prefecture" dynamo:"gha_prefecture"`
	GHAAddress1                   MultiLang `json:"gha_address_1" dynamo:"gha_address_1"`
	GHAAddress2                   MultiLang `json:"gha_address_2" dynamo:"gha_address_2"`
	GHAContactTel                 string    `json:"gha_contact_tel" dynamo:"gha_contact_tel"`
	SupervisorName                string    `json:"supervisor_name" dynamo:"supervisor_name"`
	SupervisorNameKana            string    `json:"supervisor_name_kana" dynamo:"supervisor_name_kana"`
	SupervisorDivision            string    `json:"supervisor_division" dynamo:"supervisor_division"`
	SupervisorTel                 string    `json:"supervisor_tel" dynamo:"supervisor_tel"`
	SupervisorMail                string    `json:"supervisor_mail" dynamo:"supervisor_mail"`
	SiteController                string    `json:"site_controller" dynamo:"site_controller"`
	TLLoginID                     string    `json:"tl_login_id" dynamo:"tl_login_id"`
	TLLoginPassword               string    `json:"tl_login_password" dynamo:"tl_login_password"`
	PaymentCompanyName            string    `json:"payment_company_name" dynamo:"payment_company_name"`
	PaymentCompanyNameKana        string    `json:"payment_company_name_kana" dynamo:"payment_company_name_kana"`
	PaymentCompanyStaffName       string    `json:"payment_company_staff_name" dynamo:"payment_company_staff_name"`
	PaymentCompanyStaffNameKana   string    `json:"payment_company_staff_name_kana" dynamo:"payment_company_staff_name_kana"`
	PaymentCompanyDivisionName    string    `json:"payment_company_division_name" dynamo:"payment_company_division_name"`
	PaymentCompanyPosition        string    `json:"payment_company_position" dynamo:"payment_company_position"`
	PaymentCompanyTel             string    `json:"payment_company_tel" dynamo:"payment_company_tel"`
	PaymentCompanyEmail           string    `json:"payment_company_email" dynamo:"payment_company_email"`
	PaymentCompanyZipcode         string    `json:"payment_company_zipcode" dynamo:"payment_company_zipcode"`
	PaymentCompanyPrefecture      string    `json:"payment_company_prefecture" dynamo:"payment_company_prefecture"`
	PaymentCompanyAddress1        string    `json:"payment_company_address_1" dynamo:"payment_company_address_1"`
	PaymentCompanyAddress2        string    `json:"payment_company_address_2" dynamo:"payment_company_address_2"`
	ServiceTaxRoundingMethod      string    `json:"service_tax_rounding_method" dynamo:"service_tax_rounding_method"`
	ConsumptionTaxRoundingMethod  string    `json:"consumption_tax_rounding_method" dynamo:"consumption_tax_rounding_method"`
	CancellationFeeRoundingMethod string    `json:"cancellation_fee_rounding_method" dynamo:"cancellation_fee_rounding_method"`
	UseGoogleHotelAds             bool      `json:"use_google_hotel_ads" dynamo:"use_google_hotel_ads"`
	PsSalesPersonName             string    `json:"ps_sales_person_name" dynamo:"ps_sales_person_name"`
	Status                        int64     `json:"status" dynamo:"status"`
	HotelNamePsAdmin              string    `json:"hotel_name_ps_admin" dynamo:"hotel_name_ps_admin"`
	FacilityType                  string    `json:"facility_type" dynamo:"facility_type"`
	AvailableDate                 string    `json:"available_date" dynamo:"available_date"`
	InitialSettingFlg             bool      `json:"initial_setting_flg" dynamo:"initial_setting_flg"`
	InitialSettingFee             int       `json:"initial_setting_fee" dynamo:"initial_setting_fee"`
	AppliedAt                     string    `json:"applied_at" dynamo:"applied_at"`
	CreatedAt                     int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt                     int64     `json:"updated_at" dynamo:"updated_at"`
}

type MonthlyFee struct {
	ID                   string `json:"id" dynamo:"id"`
	AmountOfMoney        int    `json:"amount_of_money" dynamo:"amount_of_money"`
	IsSet                bool   `json:"is_set" dynamo:"is_set"`
	ApplyStartMonth      string `json:"apply_start_month" dynamo:"apply_start_month"`
	AmountOfMoneyChanged int    `json:"amount_of_money_changed" dynamo:"amount_of_money_changed"`
	CreatedAt            string `json:"created_at" dynamo:"created_at"`
	UpdatedAt            string `json:"updated_at" dynamo:"updated_at"`
}

type HotelGroup struct {
	ID        string `json:"id" dynamo:"id"`
	Name      string `json:"name" dynamo:"name"`
	Desc      string `json:"desc" dynamo:"desc"`
	CreatedBy string `json:"created_by" dynamo:"created_by"`
	UpdatedBy string `json:"updated_by" dynamo:"updated_by"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type HotelSubGroup struct {
	ID        string `json:"id" dynamo:"id"`
	GroupID   string `json:"group_id" dynamo:"group_id"`
	Name      string `json:"name" dynamo:"name"`
	Desc      string `json:"desc" dynamo:"desc"`
	CreatedBy string `json:"created_by" dynamo:"created_by"`
	UpdatedBy string `json:"updated_by" dynamo:"updated_by"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type BillingFeeType struct {
	FeeType   string `json:"fee_type" dynamo:"fee_type"`
	FeeName   string `json:"fee_name" dynamo:"fee_name"`
	FeeCode   string `json:"fee_code" dynamo:"fee_code"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type PaymentInfo struct {
	ID                        string     `json:"id" dynamo:"id"`
	CID                       string     `json:"cid" dynamo:"cid"`
	GroupID                   string     `json:"group_id" dynamo:"group_id"`
	SubGroupID                string     `json:"sub_group_id" dynamo:"sub_group_id"`
	PaymentCompanyName        NormalKana `json:"payment_company_name" dynamo:"payment_company_name"`
	BillingContactPersonName  NormalKana `json:"billing_contact_person_name" dynamo:"billing_contact_person_name"`
	ContactPersonDivisionName string     `json:"contact_person_division_name" dynamo:"contact_person_division_name"`
	ContactPersonTitle        string     `json:"contact_person_title" dynamo:"contact_person_title"`
	Email                     string     `json:"email" dynamo:"email"`
	Tel                       string     `json:"tel" dynamo:"tel"`
	Zip                       string     `json:"zip" dynamo:"zip"`
	SeqNumber                 string     `json:"seq_number" dynamo:"seq_number"`
	Prefecture                MultiLang  `json:"prefecture" dynamo:"prefecture"`
	Address1                  MultiLang  `json:"address1" dynamo:"address1"`
	Address2                  MultiLang  `json:"address2" dynamo:"address2"`
	CreatedBy                 string     `json:"created_by" dynamo:"created_by"`
	UpdatedBy                 string     `json:"updated_by" dynamo:"updated_by"`
	CreatedAt                 int64      `json:"created_at" dynamo:"created_at"`
	UpdatedAt                 int64      `json:"updated_at" dynamo:"updated_at"`
}

type CreditCardCompany struct {
	HotelID               string `json:"hotel_id" dynamo:"hotel_id"`
	PaymentCompany        PC     `json:"payment_company" dynamo:"payment_company"`
	PaymentCompanyHistory []PCH  `json:"payment_company_history" dynamo:"payment_company_history"`
}

type GroupHotel struct {
	HotelID                         string           `json:"hotel_id" dynamo:"hotel_id"`
	GroupID                         string           `json:"group_id" dynamo:"group_id"`
	SubGroupID                      string           `json:"sub_group_id" dynamo:"sub_group_id"`
	PaymentInfoID                   string           `json:"payment_info_id" dynamo:"payment_info_id"`
	RegistrationFormId              string           `json:"registration_form_id" dynamo:"registration_form_id"`
	OldRegistrationFormId           string           `json:"old_registration_form_id" dynamo:"old_registration_form_id"`
	KeywordMstSeq                   string           `json:"keyword_mst_seq" dynamo:"keyword_mst_seq"`
	FacilityName                    MultiLang        `json:"facility_name" dynamo:"facility_name"`
	FacilityCode                    string           `json:"facility_code" dynamo:"facility_code"`
	FacilityType                    string           `json:"facility_type" dynamo:"facility_type"`
	ChangeFacilityTypeDate          string           `json:"change_facility_type_date" dynamo:"change_facility_type_date"`
	PermissionNumber                string           `json:"permission_number" dynamo:"permission_number"`
	ContractorType                  string           `json:"contractor_type" dynamo:"contractor_type"`
	CompanyName                     NormalKana       `json:"company_name" dynamo:"company_name"`
	CompanyRepresentativeName       NormalKana       `json:"company_representative_name" dynamo:"company_representative_name"`
	CompanyRepresentativeTitle      string           `json:"company_representative_title" dynamo:"company_representative_title"`
	CompanyRepresentativeContactTel string           `json:"company_representative_contact_tel" dynamo:"company_representative_contact_tel"`
	CompanyRepresentativeEmail      string           `json:"company_representative_email" dynamo:"company_representative_email"`
	Tel                             string           `json:"tel" dynamo:"tel"`
	Zip                             string           `json:"zip" dynamo:"zip"`
	Prefecture                      MultiLang        `json:"prefecture" dynamo:"prefecture"`
	Address1                        MultiLang        `json:"address1" dynamo:"address1"`
	Address2                        MultiLang        `json:"address2" dynamo:"address2"`
	SupervisorName                  NormalKana       `json:"supervisor_name" dynamo:"supervisor_name"`
	SupervisorDivisionName          string           `json:"supervisor_division_name" dynamo:"supervisor_division_name"`
	SupervisorContactTel            string           `json:"supervisor_contact_tel" dynamo:"supervisor_contact_tel"`
	ServiceTaxRoundingMethod        string           `json:"service_tax_rounding_method" dynamo:"service_tax_rounding_method"`
	ConsumptionTaxRoundingMethod    string           `json:"consumption_tax_rounding_method" dynamo:"consumption_tax_rounding_method"`
	CancellationFeeRoundingMethod   string           `json:"cancellation_fee_rounding_method" dynamo:"cancellation_fee_rounding_method"`
	PsincOtherServices              []string         `json:"psinc_other_services" dynamo:"psinc_other_services"`
	SiteController                  PC               `json:"site_controller" dynamo:"site_controller"`
	SiteControllerMemo              string           `json:"site_controller_memo" dynamo:"site_controller_memo"`
	PaymentCompany                  PC               `json:"payment_company" dynamo:"payment_company"`
	PaymentCompanyMemo              string           `json:"payment_company_memo" dynamo:"payment_company_memo"`
	HotelAdsCampaignID              string           `json:"hotel_ads_campaign_id" dynamo:"hotel_ads_campaign_id"`
	CommissionTripDotCOM            int64            `json:"commission_trip_dot_com" dynamo:"commission_trip_dot_com"`
	CommissionDirect                int64            `json:"commission_direct" dynamo:"commission_direct"`
	IsActive                        bool             `json:"is_active" dynamo:"is_active"`
	UseGoogleHotelAds               bool             `json:"use_google_hotel_ads" dynamo:"use_google_hotel_ads"`
	PsSalesPersonName               string           `json:"ps_sales_person_name" dynamo:"ps_sales_person_name"`
	Status                          int64            `json:"status" dynamo:"status"`
	HotelNamePsAdmin                string           `json:"hotel_name_ps_admin" dynamo:"hotel_name_ps_admin"`
	CreatedBy                       string           `json:"created_by" dynamo:"created_by"`
	UpdatedBy                       string           `json:"updated_by" dynamo:"updated_by"`
	Longitude                       string           `json:"longitude" dynamo:"longitude"`
	Latitude                        string           `json:"latitude" dynamo:"latitude"`
	EmailIssuedAt                   string           `json:"email_issued_at" dynamo:"email_issued_at"`
	CID                             string           `json:"cid" dynamo:"cid"`
	ContractNumber                  string           `json:"contract_number" dynamo:"contract_number"`
	ContractNumberHistory           []CH             `json:"contract_number_history" dynamo:"contract_number_history"`
	AppliedAt                       string           `json:"applied_at" dynamo:"applied_at"`
	CreatedAt                       int64            `json:"created_at" dynamo:"created_at"`
	UpdatedAt                       int64            `json:"updated_at" dynamo:"updated_at"`
	GHAZipcode                      string           `json:"gha_zipcode" dynamo:"gha_zipcode"`
	GHAPrefecture                   MultiLang        `json:"gha_prefecture" dynamo:"gha_prefecture"`
	GHAAddress1                     MultiLang        `json:"gha_address_1" dynamo:"gha_address_1"`
	GHAAddress2                     MultiLang        `json:"gha_address_2" dynamo:"gha_address_2"`
	GHAContactTel                   string           `json:"gha_contact_tel" dynamo:"gha_contact_tel"`
	GHACountryCode                  string           `json:"gha_country_code" dynamo:"gha_country_code"`
	ContractStartDate               string           `json:"contract_start_date" dynamo:"contract_start_date"`
	ContractEndDate                 string           `json:"contract_end_date" dynamo:"contract_end_date"`
	ContractCancelNoticeDate        string           `json:"contract_cancel_notice_date" dynamo:"contract_cancel_notice_date"`
	MaintenanceFeeExemptionFlag     bool             `json:"maintenance_fee_exemption_flag" dynamo:"maintenance_fee_exemption_flag"`
	PaymentCompanyHistory           []PCH            `json:"payment_company_history" dynamo:"payment_company_history"`
	PortalSiteInfoList              []PortalSiteInfo `json:"portal_site_info_list" dynamo:"portal_site_info_list"`
	InitialSettingFlg               bool             `json:"initial_setting_flg" dynamo:"initial_setting_flg"`
}

type PortalSiteInfo struct {
	PortalSiteID           string   `json:"portal_site_id" dynamo:"portal_site_id"`
	PortalSiteAreaPathList []string `json:"portal_site_area_path_list" dynamo:"portal_site_area_path_list"`
	PortalSiteTypeID       string   `json:"portal_site_type_id" dynamo:"portal_site_type_id"`
	PortalSiteTagIDList    []string `json:"portal_site_tag_id_list" dynamo:"portal_site_tag_id_list"`
	JoinDate               string   `json:"join_date" dynamo:"join_date"`
	Order                  int      `json:"order" dynamo:"order"`
}

type PortalSiteForm struct {
	ID                          string     `json:"id" dynamo:"id"`
	PortalSiteId                string     `json:"portal_site_id" dynamo:"portal_site_id"`
	Name                        MultiLang  `json:"name" dynamo:"name" validate:"required_if=Status Received" jaFieldName:"ポータルサイト名"`
	Url                         string     `json:"url" dynamo:"url" validate:"omitempty,url,required_if=Status Received" jaFieldName:"ポータルサイトURL"`
	ContractCompanyName         NormalKana `json:"contract_company_name" dynamo:"contract_company_name" validate:"required_if=Status Received" jaFieldName:"契約会社名"`
	RepresentativeName          NormalKana `json:"representative_name" dynamo:"representative_name" validate:"required_if=Status Received" jaFieldName:"代表者名（契約会社）"`
	RepresentativePosition      string     `json:"representative_position" dynamo:"representative_position" validate:"required_if=Status Received" jaFieldName:"役職（契約会社）"`
	Tel                         string     `json:"tel" dynamo:"tel" validate:"required_if=Status Received" jaFieldName:"連絡先お電話番号（契約会社）"`
	Zip                         string     `json:"zip" dynamo:"zip" validate:"omitempty,min=7,max=8,required_if=Status Received" jaFieldName:"郵便番号（契約会社）"`
	Prefecture                  string     `json:"prefecture" dynamo:"prefecture" validate:"required_if=Status Received" jaFieldName:"都道府県（契約会社）"`
	Address1                    string     `json:"address1" dynamo:"address1" validate:"required_if=Status Received" jaFieldName:"ご住所1（契約会社）"`
	Address2                    string     `json:"address2" dynamo:"address2"`
	ManagerName                 NormalKana `json:"manager_name" dynamo:"manager_name" validate:"required_if=Status Received" jaFieldName:"ID管理者名"`
	ManagerDepartment           string     `json:"manager_department" dynamo:"manager_department" jaFieldName:"部署名"`
	ManagerTel                  string     `json:"manager_tel" dynamo:"manager_tel" validate:"required_if=Status Received" jaFieldName:"電話番号（ID管理者）"`
	ManagerEmail                string     `json:"manager_email" dynamo:"manager_email" validate:"omitempty,email,required_if=Status Received" jaFieldName:"メールアドレス（ID管理者）"`
	PaymentCompanyName          NormalKana `json:"payment_company_name" dynamo:"payment_company_name" validate:"required_if=Status Received" jaFieldName:"御請求先会社名"`
	PaymentCompanyStaffName     NormalKana `json:"payment_company_staff_name" dynamo:"payment_company_staff_name" validate:"required_if=Status Received" jaFieldName:"御担当者名（御請求先会社）"`
	PaymentCompanyDepartment    string     `json:"payment_company_department" dynamo:"payment_company_department" jaFieldName:"部署名（御請求先会社）"`
	PaymentCompanyPosition      string     `json:"payment_company_position" dynamo:"payment_company_position" jaFieldName:"役職（御請求先会社）"`
	PaymentCompanyTel           string     `json:"payment_company_tel" dynamo:"payment_company_tel" validate:"required_if=Status Received" jaFieldName:"ご連絡先お電話番号（（御請求先会社））"`
	PaymentCompanyEmail         string     `json:"payment_company_email" dynamo:"payment_company_email" validate:"omitempty,email,required_if=Status Received" jaFieldName:"連絡先メールアドレス（御請求先会社）"`
	PaymentCompanyZipcode       string     `json:"payment_company_zipcode" dynamo:"payment_company_zipcode" validate:"omitempty,min=7,max=8,required_if=Status Received" jaFieldName:"郵便番号（御請求先会社）"`
	PaymentCompanyPrefecture    string     `json:"payment_company_prefecture" dynamo:"payment_company_prefecture" validate:"required_if=Status Received" jaFieldName:"都道府県（御請求先会社）"`
	PaymentCompanyAddress1      string     `json:"payment_company_address1" dynamo:"payment_company_address1" validate:"required_if=Status Received" jaFieldName:"ご住所1（御請求先会社）"`
	PaymentCompanyAddress2      string     `json:"payment_company_address2" dynamo:"payment_company_address2"`
	PortalSiteNameFoPsAdmin     string     `json:"portal_site_name_for_ps_admin" dynamo:"portal_site_name_for_ps_admin" validate:"required_if=Status NotReceived" jaFieldName:"ポータルサイト名（PS用）"`
	UseStartDate                string     `json:"use_start_date" dynamo:"use_start_date" validate:"required_if=Status NotReceived" jaFieldName:"利用開始日"`
	PortalSiteCommissionRate    int        `json:"portal_site_commission_rate" dynamo:"portal_site_commission_rate" validate:"min=0,max=100" jaFieldName:"ポータル側手数料"`
	PortalSiteInitialSettingFee int        `json:"portal_site_initial_setting_fee" dynamo:"portal_site_initial_setting_fee" validate:"min=0" jaFieldName:"ポータル初期設定費"`
	PsSalesPersonName           string     `json:"ps_sales_person_name" dynamo:"ps_sales_person_name" validate:"required_if=Status NotReceived" jaFieldName:"PS営業担当者名"`
	Status                      string     `json:"status" dynamo:"status" validate:"oneof=NotReceived Received Resistered" jaFieldName:"ステータス"`
	AppliedAt                   string     `json:"applied_at" dynamo:"applied_at" validate:"omitempty,datetime=2006-01-02 15:04:05,required_if=Status Resistered" jaFieldName:"申込み受付日時"`
	IsInvalidManagerEmail       bool       `json:"is_invalid_manager_email" dynamo:"is_invalid_manager_email"`
	CreatedAt                   string     `json:"created_at" dynamo:"created_at"`
	UpdatedAt                   string     `json:"updated_at" dynamo:"updated_at"`
}

type PortalSite struct {
	ID                              string     `json:"id" dynamo:"id"`
	Name                            MultiLang  `json:"name" dynamo:"name" validate:"required" jaFieldName:"ポータルサイト名"`
	Url                             string     `json:"url" dynamo:"url" validate:"required,url" jaFieldName:"ポータルサイトURL"`
	ImageUrl                        string     `json:"image_url" dynamo:"image_url"`
	CompanyName                     NormalKana `json:"company_name" dynamo:"company_name" validate:"required" jaFieldName:"契約会社名"`
	CompanyRepresentativeName       NormalKana `json:"company_representative_name" dynamo:"company_representative_name" validate:"required" jaFieldName:"代表者名"`
	CompanyRepresentativePosition   string     `json:"company_representative_position" dynamo:"company_representative_position" validate:"required" jaFieldName:"役職"`
	Tel                             string     `json:"tel" dynamo:"tel" validate:"required" jaFieldName:"連絡先電話番号"`
	Zip                             string     `json:"zip" dynamo:"zip" validate:"min=7,max=8,required" jaFieldName:"郵便番号"`
	Prefecture                      MultiLang  `json:"prefecture" dynamo:"prefecture" validate:"required" jaFieldName:"都道府県"`
	Address1                        MultiLang  `json:"address1" dynamo:"address1" validate:"required" jaFieldName:"住所1"`
	Address2                        MultiLang  `json:"address2" dynamo:"address2" jaFieldName:""`
	ManagerName                     NormalKana `json:"manager_name" dynamo:"manager_name" validate:"required" jaFieldName:"ID管理者名"`
	ManagerDepartment               string     `json:"manager_department" dynamo:"manager_department" jaFieldName:"部署名（ID管理者）"`
	ManagerTel                      string     `json:"manager_tel" dynamo:"manager_tel" validate:"required" jaFieldName:"電話番号（ID管理者）"`
	ManagerEmail                    string     `json:"manager_email" dynamo:"manager_email" validate:"email,required" jaFieldName:"メールアドレス（ID管理者）"`
	LoginPassword                   string     `json:"login_password"`
	PaymentInfoID                   string     `json:"payment_info_id" dynamo:"payment_info_id" validate:"required" jaFieldName:"請求情報"`
	CID                             string     `json:"cid" dynamo:"cid"`
	ContractNumber                  string     `json:"contract_number" dynamo:"contract_number"`
	AppliedAt                       string     `json:"applied_at" dynamo:"applied_at" validate:"omitempty,datetime=2006-01-02 15:04:05" jaFieldName:"申込み受付日時"`
	IssueNoticeDate                 string     `json:"issue_notice_date" dynamo:"issue_notice_date" validate:"omitempty,datetime=2006-01-02" jaFieldName:"発行通知日"`
	UseStartDate                    string     `json:"use_start_date" dynamo:"use_start_date" jaFieldName:"利用開始日"`
	ContractScheduledEndDate        string     `json:"contract_scheduled_end_date" dynamo:"contract_scheduled_end_date" validate:"omitempty,datetime=2006-01-02" jaFieldName:"契約終了日"`
	ContractCancelNoticeDate        string     `json:"contract_cancel_notice_date" dynamo:"contract_cancel_notice_date" validate:"omitempty,datetime=2006-01-02" jaFieldName:"解約通知日"`
	PsSalesPersonName               string     `json:"ps_sales_person_name" dynamo:"ps_sales_person_name" jaFieldName:"PS営業担当者名"`
	PortalSiteFormID                string     `json:"portal_site_form_id" dynamo:"portal_site_form_id" jaFieldName:"ポータルサイトフォームID"`
	PageLanguages                   []string   `json:"page_languages" dynamo:"page_languages"`
	IsActive                        bool       `json:"is_active" dynamo:"is_active"`
	PortalSiteCommissionRate        int        `json:"portal_site_commission_rate" dynamo:"portal_site_commission_rate" validate:"min=0,max=100" jaFieldName:"ポータル側手数料"`
	IsSet                           bool       `json:"is_set" dynamo:"is_set"`
	PortalSiteCommissionRateChanged int        `json:"portal_site_commission_rate_changed" dynamo:"portal_site_commission_rate_changed" validate:"min=0,max=100" jaFieldName:"変更後ポータル側手数料"`
	ApplyStartDate                  string     `json:"apply_start_date" dynamo:"apply_start_date" validate:"required_if=IsSet true,omitempty,datetime=2006-01-02" jaFieldName:"適用開始日付"`
	LastLogin                       string     `json:"last_login" dynamo:"last_login"`
	CreatedAt                       string     `json:"created_at" dynamo:"created_at"`
	UpdatedAt                       string     `json:"updated_at" dynamo:"updated_at"`
}

type PortalSiteArea struct {
	ID           string    `json:"id" dynamo:"id"`
	PortalSiteID string    `json:"portal_site_id" dynamo:"portal_site_id"`
	Name         MultiLang `json:"name" dynamo:"name" validate:"required" jaFieldName:"名称"`
	Path         string    `json:"path" dynamo:"path"`
	FullName     string    `json:"full_name"`
	Order        int       `json:"order" dynamo:"order" validate:"required_without=ID" jaFieldName:"優先度"`
	CreatedAt    string    `json:"created_at" dynamo:"created_at"`
	UpdatedAt    string    `json:"updated_at" dynamo:"updated_at"`
}

type PortalSiteType struct {
	ID           string    `json:"id" dynamo:"id"`
	PortalSiteID string    `json:"portal_site_id" dynamo:"portal_site_id"`
	Name         MultiLang `json:"name" dynamo:"name" validate:"required" jaFieldName:"名称"`
	CreatedAt    string    `json:"created_at" dynamo:"created_at"`
	UpdatedAt    string    `json:"updated_at" dynamo:"updated_at"`
}

type PortalSiteTag struct {
	ID           string    `json:"id" dynamo:"id"`
	PortalSiteID string    `json:"portal_site_id" dynamo:"portal_site_id"`
	Name         MultiLang `json:"name" dynamo:"name" validate:"required" jaFieldName:"名称"`
	Category     string    `json:"category" dynamo:"category" validate:"required,oneof=hotel room" jaFieldName:"カテゴリー"`
	CreatedAt    string    `json:"created_at" dynamo:"created_at"`
	UpdatedAt    string    `json:"updated_at" dynamo:"updated_at"`
}

type PortalSiteTagForRoom struct {
	PortalSiteID string   `json:"portal_site_id" dynamo:"portal_site_id"`
	TagIDs       []string `json:"tag_ids" dynamo:"tag_ids"`
}

type PortalSiteSpecialCode struct {
	ID           string `json:"id" dynamo:"id"`
	PortalSiteID string `json:"portal_site_id" dynamo:"portal_site_id"`
	Code         string `json:"code" dynamo:"code" validate:"omitempty,min=6,max=10,alphanum,required_without=ID" jaFieldName:"コード"`
	Description  string `json:"description" dynamo:"description"`
	StartDate    string `json:"start_date" dynamo:"start_date" validate:"omitempty,datetime=2006-01-02" jaFieldName:"利用期間開始日"`
	EndDate      string `json:"end_date" dynamo:"end_date" validate:"omitempty,datetime=2006-01-02" jaFieldName:"利用期間終了日"`
	CreatedAt    string `json:"created_at" dynamo:"created_at"`
	UpdatedAt    string `json:"updated_at" dynamo:"updated_at"`
}

type PortalSiteDefaultSetting struct {
	PortalSiteID          string `json:"portal_site_id" dynamo:"portal_site_id"`
	SearchDay             int    `json:"search_day" dynamo:"search_day"`
	SearchNumberOfNight   int    `json:"search_number_of_night" dynamo:"search_number_of_night"`
	SearchNumberOfPax     int    `json:"search_number_of_pax" dynamo:"search_number_of_pax"`
	SearchNumberOfRoom    int    `json:"search_number_of_room" dynamo:"search_number_of_room"`
	MaxSearchableAdult    int    `json:"max_searchable_adult"`
	MaxSearchableChildren int    `json:"max_searchable_children"`
	MaxRoomsPerBooking    int    `json:"max_rooms_per_booking"`
}

type PortalSiteSpecialCodeForPlan struct {
	PortalSiteID string   `json:"portal_site_id" dynamo:"portal_site_id"`
	CodeIDs      []string `json:"code_ids" dynamo:"code_ids"`
}

type PortalSiteView struct {
	PortalSiteID string `json:"portal_site_id" dynamo:"portal_site_id"`
	Token        string `json:"token" dynamo:"token"`
	Date         string `json:"date" dynamo:"date"`
	Count        int    `json:"count" dynamo:"count"`
}

type PortalSiteViewReport struct {
	PortalSiteID string `json:"portal_site_id" dynamo:"portal_site_id"`
	Date         string `json:"date" dynamo:"date"`
	Day          string `json:"day" dynamo:"day"`
	ViewCount    int    `json:"view_count" dynamo:"view_count"`
	UUCount      int    `json:"uu_count" dynamo:"uu_count"`
}

type PCH struct {
	PlanType             string `json:"plan_type" dynamo:"plan_type"`
	RequestDate          string `json:"request_date" dynamo:"request_date"`
	StartDate            string `json:"start_date" dynamo:"start_date"`
	CancelRequestDate    string `json:"cancel_request_date" dynamo:"cancel_request_date"`
	StopDate             string `json:"stop_date" dynamo:"stop_date"`
	ExpireDate           string `json:"expire_date" dynamo:"expire_date"`
	HotelPayFacilityCode string `json:"hotel_pay_facility_code" dynamo:"hotel_pay_facility_code"`
	IsExpireDateOriginal bool   `json:"is_expire_date_original" dynamo:"is_expire_date_original"`
}

type CH struct {
	Code      string `json:"code" dynamo:"code"`
	CreatedAt string `json:"created_at" dynamo:"created_at"`
}

type RegistrationFormPassword struct {
	ID        string `json:"id" dynamo:"id"`
	Password  string `json:"password" dynamo:"password"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type NormalKana struct {
	Normal string `json:"normal" dynamo:"normal"`
	Kana   string `json:"kana" dynamo:"kana"`
}

type PlanOrder []PlanOrderList

type PlanOrderList struct {
	ID    string `json:"id" dynamo:"id"`
	Order int64  `json:"order" dynamo:"order"`
}

type Mail struct {
	ServiceName  string            `json:"serviceName" dynamo:"serviceName"`
	TopicID      string            `json:"topicId" dynamo:"topicId"`
	SubscriberID string            `json:"subscriberId" dynamo:"subscriberId"`
	Template     Template          `json:"template" dynamo:"template"`
	Variables    map[string]string `json:"variables" dynamo:"variables"`
}

type Template struct {
	From    string   `json:"from" dynamo:"from"`
	To      string   `json:"to" dynamo:"to"`
	Cc      []string `json:"cc" dynamo:"cc"`
	Bcc     []string `json:"bcc" dynamo:"bcc"`
	Subject string   `json:"subject" dynamo:"subject"`
	Text    string   `json:"text" dynamo:"text"`
	HTML    string   `json:"html" dynamo:"html"`
}

// invoice_id is CID of payment table
// if partially paid then balance for next month is stored in balance
type Invoice struct {
	ID                     string `json:"id" dynamo:"id"`
	PortalSiteID           string `json:"portal_site_id" dynamo:"portal_site_id"`
	Cid                    string `json:"cid" dynamo:"cid"`
	BillNumber             string `json:"bill_number" dynamo:"bill_number"`
	BillingCompanyCode     string `json:"billing_company_code" dynamo:"billing_company_code"` // CID
	HotelID                string `json:"hotel_id" dynamo:"hotel_id"`
	TargetMonth            string `json:"target_month" dynamo:"target_month"`                   // bill title 件名 column 3
	TargetMonthCategory    string `json:"target_month_category" dynamo:"target_month_category"` // sort key
	PaymentDeadline        string `json:"payment_deadline" dynamo:"payment_deadline"`           // bill title 件名 column 3
	ClosingDate            string `json:"closing_date" dynamo:"closing_date"`                   // Payment deadline date 入金期限 column 4
	ReservationCount       string `json:"reservation_count" dynamo:"reservation_count"`
	CancellationCount      string `json:"cancellation_count" dynamo:"cancellation_count"`
	ActualReservationCount string `json:"actual_reservation_count" dynamo:"actual_reservation_count"`
	ItemName               string `json:"item_name" dynamo:"item_name"`
	ItemCode               string `json:"item_code" dynamo:"item_code"`
	TaxClass               string `json:"tax_class" dynamo:"tax_class"`
	TaxPercent             string `json:"tax_percent" dynamo:"tax_percent"`
	BilledAmountWithoutTax string `json:"billed_amount_without_tax" dynamo:"billed_amount_without_tax"` //今回請求金額（税抜） Billed amount this time (excluding tax)
	BilledAmountWithTax    string `json:"billed_amount_with_tax" dynamo:"billed_amount_with_tax"`       //今回請求金額（税抜） Billed amount this time (excluding tax)
	BilledAmountTax        string `json:"billed_amount_tax" dynamo:"billed_amount_tax"`                 //今回請求金額（税込）Billed amount this time (tax included)
	Status                 string `json:"status" dynamo:"status"`
	BillingMonth           string `json:"billing_month" dynamo:"billing_month"`
	DipositAmount          string `json:"diposit_amount" dynamo:"diposit_amount"`
	DipositDate            string `json:"diposit_date" dynamo:"diposit_date"`
	IsFullPaid             bool   `json:"is_full_paid" dynamo:"is_full_paid"`
	Balance                string `json:"balance" dynamo:"balance"`
	Category               string `json:"category" dynamo:"category"`           // item code by developer
	CategoryCode           string `json:"category_code" dynamo:"category_code"` // item code by 3rd party
	CategoryName           string `json:"category_name" dynamo:"category_name"` // item name
	ContractNumber         string `json:"contract number" dynamo:"contract number"`
	KeywordMstSeq          string `json:"keyword_mst_seq" dynamo:"keyword_mst_seq"`
	HotelName              string `json:"hotel_name" dynamo:"hotel_name"`
	PortalSiteName         string `json:"portal_site_name" dynamo:"portal_site_name"`
	CheckIn                string `json:"check_in" dynamo:"check_in"`
	CheckOut               string `json:"check_out" dynamo:"check_out"`
	Pax                    int    `json:"pax" dynamo:"pax"`
	ForwardTotalCharge     string `json:"forward_total_charge" dynamo:"forward_total_charge"` // PS管理画面の経理計上データcsvダウンロードでのみ使用する
	CreatedAt              string `json:"created_at" dynamo:"created_at"`
	UpdatedAt              string `json:"updated_at" dynamo:"updated_at"`
	UpdatedBy              string `json:"updated_by" dynamo:"updated_by"`
}

type PsincPayment struct {
	ID               string `json:"id" dynamo:"id"`
	PortalSiteID     string `json:"portal_site_id" dynamo:"portal_site_id"`
	HotelID          string `json:"hotel_id" dynamo:"hotel_id"`
	ReferenceID      string `json:"reference_id" dynamo:"reference_id"`
	CheckIn          string `json:"check_in" dynamo:"check_in"`
	CheckOut         string `json:"check_out" dynamo:"check_out"`
	Night            int    `json:"night" dynamo:"night"`
	HotelName        string `json:"hotel_name" dynamo:"hotel_name"`
	BookerName       string `json:"bookger_name" dynamo:"bookger_name"`
	BookingCharge    int    `json:"booking_charge" dynamo:"booking_charge"`
	Commission       int    `json:"comission" dynamo:"comission"`
	PaymentDeadline  string `json:"payment_deadline" dynamo:"payment_deadline"`
	NotificationDate string `json:"notification_date" dynamo:"notification_date"`
	PaymentReportURL string `json:"payment_report_url" dynamo:"payment_report_url"`
	CreatedAt        string `json:"created_at" dynamo:"created_at"`
	UpdatedAt        string `json:"updated_at" dynamo:"updated_at"`
}

type Option struct {
	ID                        string           `json:"id" dynamo:"id"`
	HotelID                   string           `json:"hotel_id" dynamo:"hotel_id"`
	Name                      MultiLang        `json:"name" dynamo:"name"`
	InternalMemo              string           `json:"internal_memo" dynamo:"internal_memo"`
	Code                      string           `json:"code" dynamo:"code"`     //auto generated
	Status                    string           `json:"status" dynamo:"status"` //OnSale, StopSale
	Description               MultiLang        `json:"description" dynamo:"description"`
	Unit                      string           `json:"unit" dynamo:"unit"` // counter for item like bottle/person
	PricePerUnit              float64          `json:"price_per_unit" dynamo:"price_per_unit"`
	Images                    []Image          `json:"images" dynamo:"images"`
	OptionCount               int64            `json:"option_count" dynamo:"option_count"`
	MinimumOptionCount        int64            `json:"minimum_option_count" dynamo:"minimum_option_count"` // only used for TL-Lincoln
	OptionCountUpdateDateTime int64            `json:"option_count_update_date_time" dynamo:"option_count_update_date_time"`
	OptionCountPerDay         int64            `json:"option_count_per_day" dynamo:"option_count_per_day"`
	OptionCountPerBooking     int64            `json:"option_count_per_booking" dynamo:"option_count_per_booking"`
	AvailablePairs            []int64          `json:"available_pairs" dynamo:"available_pairs"`
	AvailableDays             []string         `json:"available_days" dynamo:"available_days"`                     // Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
	Repetition                string           `json:"repetition" dynamo:"repetition"`                             // Once, Repeate
	AvailableFor              string           `json:"available_for" dynamo:"available_for"`                       // All, AdultOnly, ChildOnly, any number [1/2/3/4 one number]
	AvailablePaymentMethod    []string         `json:"available_payment_method" dynamo:"available_payment_method"` // AtHotel, CreditCard
	CutOffDays                int64            `json:"cut_off_days" dynamo:"cut_off_days"`
	DisplayFlag               bool             `json:"display_flag" dynamo:"display_flag"`
	DisplayStartDate          string           `json:"display_start_date" dynamo:"display_start_date"`
	DisplayEndDate            string           `json:"display_end_date" dynamo:"display_end_date"`
	SalesFlag                 bool             `json:"sales_flag" dynamo:"sales_flag"`
	SalesStartDate            string           `json:"sales_start_date" dynamo:"sales_start_date"`
	SalesEndDate              string           `json:"sales_end_date" dynamo:"sales_end_date"`
	PlanList                  []string         `json:"plan_list" dynamo:"plan_list"`
	Cancellationcharge100     bool             `json:"cancellation_charge_100" dynamo:"cancellation_charge_100"`
	Order                     int64            `json:"order" dynamo:"order"`
	QuestionsForCustomer      []OptionQuestion `json:"questions_for_customer" dynamo:"questions_for_customer"`
	CreatedAt                 int64            `json:"created_at" dynamo:"created_at"`
	UpdatedAt                 int64            `json:"updated_at" dynamo:"updated_at"`
	IsFuturePriceSet          bool             `json:"is_future_price_set" dynamo:"is_future_price_set"`
	FuturePriceStartDate      string           `json:"future_price_start_date" dynamo:"future_price_start_date"`
	FuturePricePerUnit        float64          `json:"future_price_per_unit" dynamo:"future_price_per_unit"`
	IsCheckoutDateIncluded    bool             `json:"is_checkout_date_included" dynamo:"is_checkout_date_included"`
	RemainingOptionCount      int64            `json:"remaining_option_count" dynamo:"remaining_option_count"`
	ResetOptionCount          bool             `json:"reset_option_count" dynamo:"reset_option_count"`
}

type OptionQuestion struct {
	ShowGuestPage bool      `json:"show_guest_page" dynamo:"show_guest_page"`
	Question      MultiLang `json:"question" dynamo:"question"`
}

type OptionQuestionAnswer struct {
	ShowGuestPage bool      `json:"show_guest_page" dynamo:"show_guest_page"`
	Question      MultiLang `json:"question" dynamo:"question"`
	Answer        string    `json:"answer" dynamo:"answer"`
}

type AdminSiteOption struct {
	ID                        string           `json:"id" dynamo:"id"`
	HotelID                   string           `json:"hotel_id" dynamo:"hotel_id"`
	Name                      MultiLang        `json:"name" dynamo:"name"`
	InternalMemo              string           `json:"internal_memo" dynamo:"internal_memo"`
	Code                      string           `json:"code" dynamo:"code"`     //auto generated
	Status                    string           `json:"status" dynamo:"status"` //OnSale, StopSale
	Description               MultiLang        `json:"description" dynamo:"description"`
	Unit                      string           `json:"unit" dynamo:"unit"` // counter for item like bottle/person
	PricePerUnit              float64          `json:"price_per_unit" dynamo:"price_per_unit"`
	Images                    []Image          `json:"images" dynamo:"images"`
	MinimumOptionCount        int64            `json:"minimum_option_count" dynamo:"minimum_option_count"` // only used for TL-Lincoln
	OptionCount               int64            `json:"option_count" dynamo:"option_count"`
	OptionCountUpdateDateTime int64            `json:"option_count_update_date_time" dynamo:"option_count_update_date_time"` //backend filled
	RemainingOptionCount      int64            `json:"remaining_option_count" dynamo:"remaining_option_count"`
	OptionCountPerDay         int64            `json:"option_count_per_day" dynamo:"option_count_per_day"`
	OptionCountPerBooking     int64            `json:"option_count_per_booking" dynamo:"option_count_per_booking"`
	AvailablePairs            []int64          `json:"available_pairs" dynamo:"available_pairs"`
	AvailableDays             []string         `json:"available_days" dynamo:"available_days"`                     // Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
	Repetition                string           `json:"repetition" dynamo:"repetition"`                             // Once, Repeate
	AvailableFor              string           `json:"available_for" dynamo:"available_for"`                       // All, AdultOnly, ChildOnly, AllOptions
	AvailablePaymentMethod    []string         `json:"available_payment_method" dynamo:"available_payment_method"` // AtHotel, CreditCard
	CutOffDays                int64            `json:"cut_off_days" dynamo:"cut_off_days"`
	DisplayFlag               bool             `json:"display_flag" dynamo:"display_flag"`
	DisplayStartDate          string           `json:"display_start_date" dynamo:"display_start_date"`
	DisplayEndDate            string           `json:"display_end_date" dynamo:"display_end_date"`
	SalesFlag                 bool             `json:"sales_flag" dynamo:"sales_flag"`
	SalesStartDate            string           `json:"sales_start_date" dynamo:"sales_start_date"`
	SalesEndDate              string           `json:"sales_end_date" dynamo:"sales_end_date"`
	PlanList                  []string         `json:"plan_list" dynamo:"plan_list"`
	Cancellationcharge100     bool             `json:"cancellation_charge_100" dynamo:"cancellation_charge_100"`
	Order                     int64            `json:"order" dynamo:"order"`
	CreatedAt                 int64            `json:"created_at" dynamo:"created_at"`
	UpdatedAt                 int64            `json:"updated_at" dynamo:"updated_at"`
	QuestionsForCustomer      []OptionQuestion `json:"questions_for_customer" dynamo:"questions_for_customer"`
	IsFuturePriceSet          bool             `json:"is_future_price_set" dynamo:"is_future_price_set"`
	FuturePriceStartDate      string           `json:"future_price_start_date" dynamo:"future_price_start_date"`
	FuturePricePerUnit        float64          `json:"future_price_per_unit" dynamo:"future_price_per_unit"`
	IsCheckoutDateIncluded    bool             `json:"is_checkout_date_included" dynamo:"is_checkout_date_included"`
	ResetOptionCount          bool             `json:"reset_option_count" dynamo:"reset_option_count"`
}

type GuestSiteOption struct {
	ID                          string           `json:"id" dynamo:"id"`
	HotelID                     string           `json:"hotel_id" dynamo:"hotel_id"`
	Name                        MultiLang        `json:"name" dynamo:"name"`
	InternalMemo                string           `json:"internal_memo" dynamo:"internal_memo"`
	Code                        string           `json:"code" dynamo:"code"`     //auto generated
	Status                      string           `json:"status" dynamo:"status"` //OnSale, StopSale
	Description                 MultiLang        `json:"description" dynamo:"description"`
	Unit                        string           `json:"unit" dynamo:"unit"` // counter for item like bottle/person
	PricePerUnit                float64          `json:"price_per_unit" dynamo:"price_per_unit"`
	Images                      []Image          `json:"images" dynamo:"images"`
	OptionCount                 int64            `json:"option_count" dynamo:"option_count"`
	OptionCountUpdateDateTime   int64            `json:"option_count_update_date_time" dynamo:"option_count_update_date_time"` // backend filled
	RemainingOptionCount        int64            `json:"remaining_option_count" dynamo:"remaining_option_count"`
	RemainingOptionCounts       []int64          `json:"remaining_option_counts" dynamo:"remaining_option_counts"`
	RemainingOptionCountsPerDay []int64          `json:"remaining_option_counts_per_day" dynamo:"remaining_option_counts_per_day"`
	OptionCountPerDay           int64            `json:"option_count_per_day" dynamo:"option_count_per_day"`
	OptionCountPerBooking       int64            `json:"option_count_per_booking" dynamo:"option_count_per_booking"`
	AvailablePairs              []int64          `json:"available_pairs" dynamo:"available_pairs"`
	AvailableDays               []string         `json:"available_days" dynamo:"available_days"`                     // Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
	Repetition                  string           `json:"repetition" dynamo:"repetition"`                             // Once, Repeate
	AvailableFor                string           `json:"available_for" dynamo:"available_for"`                       // All, AdultOnly, ChildOnly, AllOptions
	AvailablePaymentMethod      []string         `json:"available_payment_method" dynamo:"available_payment_method"` // AtHotel, CreditCard
	CutOffDays                  int64            `json:"cut_off_days" dynamo:"cut_off_days"`
	DisplayFlag                 bool             `json:"display_flag" dynamo:"display_flag"`
	DisplayStartDate            string           `json:"display_start_date" dynamo:"display_start_date"`
	DisplayEndDate              string           `json:"display_end_date" dynamo:"display_end_date"`
	SalesFlag                   bool             `json:"sales_flag" dynamo:"sales_flag"`
	SalesStartDate              string           `json:"sales_start_date" dynamo:"sales_start_date"`
	SalesEndDate                string           `json:"sales_end_date" dynamo:"sales_end_date"`
	PlanList                    []string         `json:"plan_list" dynamo:"plan_list"`
	Cancellationcharge100       bool             `json:"cancellation_charge_100" dynamo:"cancellation_charge_100"`
	Order                       int64            `json:"order" dynamo:"order"`
	CreatedAt                   int64            `json:"created_at" dynamo:"created_at"`
	UpdatedAt                   int64            `json:"updated_at" dynamo:"updated_at"`
	QuestionsForCustomer        []OptionQuestion `json:"questions_for_customer" dynamo:"questions_for_customer"`
	IsFuturePriceSet            bool             `json:"is_future_price_set" dynamo:"is_future_price_set"`
	FuturePriceStartDate        string           `json:"future_price_start_date" dynamo:"future_price_start_date"`
	FuturePricePerUnit          float64          `json:"future_price_per_unit" dynamo:"future_price_per_unit"`
	IsCheckoutDateIncluded      bool             `json:"is_checkout_date_included" dynamo:"is_checkout_date_included"`
}

type OptionList struct {
	ID                    string                 `json:"id" dynamo:"id"`
	Unit                  string                 `json:"unit" dynamo:"unit"`
	PricesPerUnit         []float64              `json:"prices_per_unit" dynamo:"prices_per_unit"`
	CountPerNight         []int64                `json:"count_per_night" dynamo:"count_per_night"`
	Cancellationcharge100 bool                   `json:"cancellation_charge_100" dynamo:"cancellation_charge_100"`
	Name                  MultiLang              `json:"name" dynamo:"name"`
	Description           MultiLang              `json:"description" dynamo:"description"`
	Code                  string                 `json:"code" dynamo:"code"`
	Repetition            string                 `json:"repetition" dynamo:"repetition"` // Once, Repeate
	ConsumptionCharge     float64                `json:"consumption_charge" dynamo:"consumption_charge"`
	OptionQuestionAnswer  []OptionQuestionAnswer `json:"option_question_answer" dynamo:"option_question_answer"`
}

type BETLHotelIDMap struct {
	BEID      string `json:"be_id" dynamo:"be_id"`
	TLID      string `json:"tl_id" dynamo:"tl_id"`
	CreatedAt int64  `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64  `json:"updated_at" dynamo:"updated_at"`
}

type Amenity struct {
	ID        string    `json:"id" dynamo:"id"`
	Name      MultiLang `json:"name" dynamo:"name"`
	TLID      string    `json:"tl_id" dynamo:"tl_id"`
	CreatedAt int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64     `json:"updated_at" dynamo:"updated_at"`
}

type Facility struct {
	ID        string    `json:"id" dynamo:"id"`
	Name      MultiLang `json:"name" dynamo:"name"`
	TLID      string    `json:"tl_id" dynamo:"tl_id"`
	CreatedAt int64     `json:"created_at" dynamo:"created_at"`
	UpdatedAt int64     `json:"updated_at" dynamo:"updated_at"`
}

type HotelPayPlan struct {
	Name        string  `json:"name" dynamo:"name"`
	Charge      float64 `json:"charge" dynamo:"charge"`
	Description string  `json:"description" dynamo:"description"`
	CreatedAt   int64   `json:"created_at" dynamo:"created_at"`
	UpdatedAt   int64   `json:"updated_at" dynamo:"updated_at"`
}

type RankPrice struct {
	HotelID         string  `json:"hotel_id" dynamo:"hotel_id"`
	PlanRoomRankPax string  `json:"plan_room_rank_pax" dynamo:"plan_room_rank_pax"`
	PlanID          string  `json:"plan_id" dynamo:"plan_id"`
	RoomID          string  `json:"room_id" dynamo:"room_id"`
	RateRankID      string  `json:"rate_rank_id" dynamo:"rate_rank_id"`
	RateRankName    string  `json:"rate_rank_name" dynamo:"rate_rank_name"`
	Pax             int64   `json:"pax" dynamo:"pax"`
	Price           float64 `json:"price" dynamo:"price"`
	CreatedAt       int64   `json:"created_at" dynamo:"created_at"`
	UpdatedAt       int64   `json:"updated_at" dynamo:"updated_at"`
}

type RankPriceIndex struct {
	HotelID         string  `json:"hotel_id" dynamo:"hotel_id"`
	PlanRoomRankPax string  `json:"plan_room_rank_pax" dynamo:"plan_room_rank_pax"`
	PlanID          string  `json:"plan_id" dynamo:"plan_id"`
	RoomID          string  `json:"room_id" dynamo:"room_id"`
	RateRankID      string  `json:"rate_rank_id" dynamo:"rate_rank_id"`
	RateRankName    string  `json:"rate_rank_name" dynamo:"rate_rank_name"`
	IsPriceSet      bool    `json:"is_price_set" dynamo:"is_price_set"`
	Pax             int64   `json:"pax" dynamo:"pax"`
	Price           float64 `json:"price" dynamo:"price"`
	CreatedAt       int64   `json:"created_at" dynamo:"created_at"`
	UpdatedAt       int64   `json:"updated_at" dynamo:"updated_at"`
}

type UpdateTime struct {
	HotelID   string `json:"hotel_id" dynamo:"hotel_id"`
	UpdatedAt string `json:"updated_at" dynamo:"updated_at"`
}

func GetValidate() (*validator.Validate, ut.Translator) {
	ja := ja.New()
	uni := ut.New(ja, ja)

	trans, _ := uni.GetTranslator("ja")
	validate := validator.New()

	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		fieldName := fld.Tag.Get("jaFieldName")
		if fieldName == "-" {
			return ""
		}
		return fieldName
	})
	ja_translations.RegisterDefaultTranslations(validate, trans)

	_ = validate.RegisterTranslation("required_if", trans, registrationFunc("required_if", "{0}は必須フィールドです", false), translateFunc)

	return validate, trans
}
func translateFunc(ut ut.Translator, fe validator.FieldError) string {

	t, err := ut.T(fe.Tag(), fe.Field())
	if err != nil {
		log.Printf("warning: error translating FieldError: %#v", fe)
		return fe.(error).Error()
	}

	return t
}

func registrationFunc(tag string, translation string, override bool) validator.RegisterTranslationsFunc {
	return func(ut ut.Translator) (err error) {

		if err = ut.Add(tag, translation, override); err != nil {
			return
		}

		return
	}
}
