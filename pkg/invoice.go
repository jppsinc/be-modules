package pkg

import (
	"time"

	"github.com/yut-kt/goholiday"
	"github.com/yut-kt/goholiday/nholidays/jp"
)

// 入金期限を取得
func GetPaymentDeadline(targetBillingMonth string) (string, error) {
	billingMonth, err := time.Parse(MONTH_FORMAT, targetBillingMonth)
	if err != nil {
		return "", err
	}
	ghj := goholiday.New(jp.New())

	// 請求月の翌々月末の営業日
	endOfMonth := billingMonth.AddDate(0, 2, -1)

	// 12/31は銀行が休みなので
	if endOfMonth.Month() == time.December && ghj.IsBusinessDay(endOfMonth) {
		endOfMonth = endOfMonth.AddDate(0, 0, -1)
	}
	for !ghj.IsBusinessDay(endOfMonth) {
		endOfMonth = endOfMonth.AddDate(0, 0, -1)
	}

	return endOfMonth.Format(DATE_FORMAT), nil
}
