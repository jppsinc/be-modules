package pkg

import (
	"fmt"
	"net/http"
	"reflect"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
)

func ApiResponsePlanCSV(status int, body interface{}, minPax int64, maxPax int64) (events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type",
		"Content-Type":                 "text/plain",
	}
	// fetch the elements form interface
	s := reflect.ValueOf(body).Elem()
	// find the type of element
	typeOfIface := s.Kind()
	// struct empty
	// slice 1 element
	// slice 3 element
	var buff1 string
	var buff2 string
	// if it is an slice of struct
	if typeOfIface == reflect.Slice {
		// iterate the slice of structs
		for i := 0; i < s.Len(); i++ {
			// for each struct the order of elements is fixed so we can use index to fetch
			// elecents and perform desired operation on them
			var rc string
			var buff1 string
			p := s.Index(i).Elem()
			// fetch first element and convert it to string wrap with double quots
			tmp := p.Field(0).Interface()
			buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)
			// fetch second element and convert it to string wrap with double quots
			tmp = p.Field(1).Field(0)
			buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

			tmp = p.Field(2).Interface()
			buff1 = fmt.Sprintf("%s\"%v-", buff1, strings.ReplaceAll(fmt.Sprintf("%s", tmp), "-", "/"))

			tmp = p.Field(3).Interface()
			buff1 = fmt.Sprintf("%s%v\",", buff1, strings.ReplaceAll(fmt.Sprintf("%s", tmp), "-", "/"))

			tmp = p.Field(4).Interface()
			tmp = fmt.Sprintf("%s", tmp)
			if tmp == "Sales" {
				tmp = "1"
			}
			if tmp == "Delete" {
				tmp = "0"
			}
			buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

			tmp = p.Field(5).Interface()
			tmp = fmt.Sprintf("%s", tmp)
			if tmp == "PerPerson" {
				for k := minPax; k <= maxPax; k++ {
					rc = fmt.Sprintf("%s\"%v\",", rc, k)
				}
				rc = strings.TrimRight(rc, ",")
				tmp = "0"
			}
			if tmp == "PerRoom" {
				rc = fmt.Sprintf("\"%v\"", "RC")
				tmp = "1"
			}
			buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

			// append the value of RC at the last
			buff1 = fmt.Sprintf("%s%v", buff1, rc)
			// append all: if multiple structs are there
			buff2 = fmt.Sprintf("%s%v\n", buff2, buff1)
		}
		resp.Body = buff2
		return resp, nil
	}

	if typeOfIface == reflect.Struct {
		var rc string
		tmp := s.Field(0).Interface()
		buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

		tmp = s.Field(1).Field(0)
		buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

		tmp = s.Field(2).Interface()
		buff1 = fmt.Sprintf("%s\"%v-", buff1, strings.ReplaceAll(fmt.Sprintf("%s", tmp), "-", "/"))

		tmp = s.Field(3).Interface()
		buff1 = fmt.Sprintf("%s%v\",", buff1, strings.ReplaceAll(fmt.Sprintf("%s", tmp), "-", "/"))

		tmp = s.Field(4).Interface()
		tmp = fmt.Sprintf("%s", tmp)
		if tmp == "Sales" {
			tmp = "1"
		}
		if tmp == "Delete" {
			tmp = "0"
		}
		buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

		tmp = s.Field(5).Interface()
		tmp = fmt.Sprintf("%s", tmp)
		if tmp == "PerPerson" {
			for k := minPax; k <= maxPax; k++ {
				rc = fmt.Sprintf("%s\"%v\",", rc, k)
			}
			rc = strings.TrimRight(rc, ",")
			tmp = "0"
		}
		if tmp == "PerRoom" {
			rc = fmt.Sprintf("\"%v\"", "RC")
			tmp = "1"
		}
		buff1 = fmt.Sprintf("%s\"%v\",", buff1, tmp)

		buff1 = fmt.Sprintf("%s%v", buff1, rc)

		resp.Body = buff1
		return resp, nil
	}

	resp.Body = string("Something went wrong in API response")
	resp.StatusCode = http.StatusInternalServerError
	return resp, nil
}

func Date2DaysInMonth(date string) (int, error) {
	layout := "2006-01"
	t, err := time.Parse(layout, date)

	firstOfMonth := time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, time.UTC)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

	return lastOfMonth.Day(), err
}
