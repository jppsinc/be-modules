// module git-codecommit.us-east-2.amazonaws.com/v1/repos/be-modules.git
module bitbucket.org/jppsinc/be-modules

go 1.15

require (
	github.com/aws/aws-lambda-go v1.19.1
	github.com/aws/aws-sdk-go v1.35.12
	github.com/elazarl/goproxy v0.0.0-20221015165544-a0805db90819 // indirect
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.11.1
	github.com/parnurzeal/gorequest v0.2.16
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/yut-kt/goholiday v1.0.4
	moul.io/http2curl v1.0.0 // indirect
)
