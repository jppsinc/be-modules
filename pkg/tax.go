package pkg

import "time"

func GetAmountAndUnit(tax ConsumptionTax) (float64, string, error) {
	amount := 0.0
	unit := ""
	p1 := tax.Period1
	p2 := tax.Period2

	// new code start
	if p2 == nil {
		amount = p1.Amount
		unit = p1.Unit
	} else {
		if p2.StartDate != nil {
			if *p2.StartDate != "" {
				// sd, _ := time.ParseInLocation()
				sd, err := time.Parse(DATE_FORMAT, *p2.StartDate)
				if err != nil {
					return 0.0, "", err
				}
				ud, err := time.Parse(DATE_FORMAT, DateString("date"))
				if err != nil {
					return 0.0, "", err
				}

				if sd.Before(ud) {
					amount = p2.Amount
					unit = p2.Unit
				} else {
					amount = p1.Amount
					unit = p1.Unit
				}
			}
		}
	}
	return amount, unit, nil
}
